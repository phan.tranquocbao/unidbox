<?php
	$classes = array();
	$classes[] = 'is-'.get_theme_mod('breadcrumb_size', 'large');
?>
<div class="<?php echo implode(' ', $classes); ?> breadcrumb_color">
	<div class="container">
			<?php flatsome_breadcrumb(); ?>
	</div>

</div>
