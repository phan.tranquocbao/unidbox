<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main>

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer>

</div>

<?php wp_footer(); ?>
<script>
	document.addEventListener("DOMContentLoaded", function(){
		let divc = document.querySelectorAll('div[style]');
		for (let i = 0, len = divc.length; i < len; i++) {
			let actdisplay = window.getComputedStyle(divc[i], null).display;
			let actclear = window.getComputedStyle(divc[i], null).clear;

			if(actdisplay == 'block' && actclear == 'both') {
				divc[i].remove();
	}
		}
			});
</script>
</body>
</html>