<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://woo.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.3.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! comments_open() ) {
	return;
}
$current_user = wp_get_current_user();
$username = $current_user->display_name;
$email = $current_user->user_email;
?>
<div id="reviews" class="woocommerce-Reviews">
	<div id="product-detail">
		<div class="wcspc-item">
			<div class="wcspc-item-inner">
				<div class="wcspc-item-thumb">
					<a href="<?php echo get_permalink($product->get_id()); ?>">
						<img width="645" height="551" src="<?php echo get_the_post_thumbnail_url($product->get_id(), 'woocommerce_thumbnail'); ?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" decoding="async">
					</a>
				</div>
				<div class="wcspc-item-info">
					<span class="wcspc-item-title">
						<?php echo $product->get_title(); ?>
					</span>
					<p class="short-des-item"><?php echo $product->get_short_description(); ?></p>
					<p class="wcspc-item-sku"><?php echo $product->get_sku(); ?></p>
					<p class="wcspc-item-install"> With Installation $0.00</p>
					<!-- <p class="wcspc-item-qty"> Qty: 1</p> -->
				</div>
				<div class="wcspc-item-price"><?php echo $product->get_price_html(); ?></div>
			</div>
		</div>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : 
		$comments = get_comments(array(
			'user_id' => get_current_user_id(),
			'post_id' => $product->get_id(),
		));
		if (!empty($comments)) {
			esc_html_e( 'You have commented on this product.', 'woocommerce' );
		}else{
		?>
		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
				$commenter    = wp_get_current_commenter();
				$comment_form = array(
					/* translators: %s is product title */
					// 'title_reply'         => have_comments() ? esc_html__( 'Add a review', 'woocommerce' ) : sprintf( esc_html__( 'Be the first to review &ldquo;%s&rdquo;', 'woocommerce' ), get_the_title() ),
					/* translators: %s is product title */
					'title_reply_to'      => esc_html__( 'Leave a Reply to %s', 'woocommerce' ),
					'title_reply_before'  => '<span id="reply-title" class="comment-reply-title">',
					'title_reply_after'   => '</span>',
					'comment_notes_after' => '',
					'label_submit'        => esc_html__( 'Submit', 'woocommerce' ),
					'logged_in_as'        => '',
					'comment_field'       => '',
				);

				$name_email_required = (bool) get_option( 'require_name_email', 1 );
				$fields              = array(
					'author' => array(
						'label'    => __( 'Full Name', 'woocommerce' ),
						'type'     => 'text',
						'value'    => $commenter['comment_author'],
						'required' => $name_email_required,
					),
					'email'  => array(
						'label'    => __( 'Email', 'woocommerce' ),
						'type'     => 'email',
						'value'    => $commenter['comment_author_email'],
						'required' => $name_email_required,
					),
				);

				$comment_form['fields'] = array();

				foreach ( $fields as $key => $field ) {
					$field_html  = '<p class="comment-form-' . esc_attr( $key ) . '">';
					$field_html .= '<label for="' . esc_attr( $key ) . '">' . esc_html( $field['label'] );

					if ( $field['required'] ) {
						$field_html .= '&nbsp;<span class="required">*</span>';
					}
					$field_html .= '</label><input id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" type="' . esc_attr( $field['type'] ) . '" value="' . esc_attr( $field['value'] ) . '" size="30" ' . ( $field['required'] ? 'required' : '' ) . ' /></p>';
					$comment_form['fields'][ $key ] = $field_html;
				}

				$account_page_url = wc_get_page_permalink( 'myaccount' );
				if ( $account_page_url ) {
					/* translators: %s opening and closing link tags respectively */
					$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( esc_html__( 'You must be %1$slogged in%2$s to post a review.', 'woocommerce' ), '<a href="' . esc_url( $account_page_url ) . '">', '</a>' ) . '</p>';
				}

				if ( wc_review_ratings_enabled() ) {
					$comment_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . esc_html__( 'Overall Rating', 'woocommerce' ) . ( wc_review_ratings_required() ? '&nbsp;<span class="required">*</span>' : '' ) . '</label><select name="rating" id="rating" required>
						<option value="">' . esc_html__( 'Rate&hellip;', 'woocommerce' ) . '</option>
						<option value="5">' . esc_html__( 'Perfect', 'woocommerce' ) . '</option>
						<option value="4">' . esc_html__( 'Good', 'woocommerce' ) . '</option>
						<option value="3">' . esc_html__( 'Average', 'woocommerce' ) . '</option>
						<option value="2">' . esc_html__( 'Not that bad', 'woocommerce' ) . '</option>
						<option value="1">' . esc_html__( 'Very poor', 'woocommerce' ) . '</option>
					</select></div>';
				}

				$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . esc_html__( 'Your Review', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label>
					<span>Comments</span>
					<textarea id="comment" name="comment" cols="45" rows="8" placeholder="What did you like or dislike? What did you use this product for?" required></textarea>
				</p>';
				$comment_form['comment_field'] .= '<p class="comment-form-file">
					<label for="review_attachment">' . esc_html__( 'Add a Photo or Video', 'woocommerce' ) . '</label>
					<br>
					<span>' . esc_html__( 'Shoppers find images and videos more helpful than text alone', 'woocommerce' ) . '</span>
					<div id="file-upload-box">
						<label for="file-input" id="file-label">
							<div id="plus-icon">+</div>
						</label>
						<input type="file" id="file-input-review-pr" name="file-input-review-pr">
						<div id="image-preview"></div>
					</div>
				</p>';


				// if(is_user_logged_in()){
				// 	$comment_form['comment_field'] .= '<p class="comment-form-author">
				// 		<label for="author">Full Name<span class="required">*</span></label>
				// 		<input id="author_review" name="author_review" type="text" value="'.$username.'" size="30" required="" disabled>
				// 	</p>';
				// 	$comment_form['comment_field'] .= '<p class="comment-form-email">
				// 		<label for="email">Email<span class="required">*</span></label>
				// 		<input id="email_review" name="email_review" type="email" value="'.$email.'" size="30" required="" disabled>
				// 	</p>';
				// }
				comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>
		<?php } ?>
	<?php else : ?>
		<p class="woocommerce-verification-required"><?php esc_html_e( 'Only logged in customers who have purchased this product may leave a review.', 'woocommerce' ); ?></p>
	<?php endif; ?>

	<div class="clear"></div>
</div>
<style type="text/css">
#file-upload-box {
  position: relative;
  width: 200px;
  height: 200px;
  border: 2px dashed #ccc;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  cursor: pointer;
}
#file-upload-box #file-input-review-pr{
	opacity: 0 !important;
	position: absolute;
	height: 100%;
	width: 100%;
}
#file-label {
  cursor: pointer;
  text-align: center;
}

#plus-icon {
  font-size: 36px;
}

#image-preview {
  margin-top: 10px;
  z-index: 999;
}

#image-preview img {
  max-width: 100%;
  max-height: 100%;
  margin-bottom: 10px;
}
</style>
<?php if (is_user_logged_in()) {
	// Change id popup when upload or change popup
	$id_popup = 7105;
	// 
    $script_text = "$(document).on('elementor/popup/show', function(event, popupId, popupType) {
        if (popupId === ".$id_popup.") {
            var selectElement = document.getElementById('rating');
            if (selectElement) {
                selectElement.style.display = 'none';
                var parentDiv = selectElement.closest('div');
                if (parentDiv) {
                    parentDiv.innerHTML += '<p class=\"stars\">\
                        <span>\
                            <a class=\"star-1\" href=\"#\">1</a>\
                            <a class=\"star-2\" href=\"#\">2</a>\
                            <a class=\"star-3\" href=\"#\">3</a>\
                            <a class=\"star-4\" href=\"#\">4</a>\
                            <a class=\"star-5\" href=\"#\">5</a>\
                        </span>\
                    </p>';
                }

                // Unbind the event to avoid multiple bindings
                $(document).off('elementor/popup/show');
            }
        }
    });

    $('#button-review-product').on('click', function() {
        $(document).trigger('elementor/popup/show', [".$id_popup.", 'popup_type']);
    });";
}else {
	$script_text = " ";
}
?>
<script type="text/javascript">


jQuery(document).ready(function($) {
	$(document).ready(function () {
		$('#file-input-review-pr').on('change', function (event) {
			event.preventDefault(); 
			var files = $(this)[0].files;
			if (files.length > 0) {
				var file = files[0];
				previewImage(file);
			}
		});

		$('#image-preview').on('click', 'img', function () {
			$(this).remove();
			$('#file-input-review-pr').val() = null;
		});

		function previewImage(file) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#image-preview').html('<img src="' + e.target.result + '" alt="Preview" data-file-name="' + file.name + '">');
			};
			reader.readAsDataURL(file);
		}
	});
	<?php echo $script_text; ?>
});
</script>