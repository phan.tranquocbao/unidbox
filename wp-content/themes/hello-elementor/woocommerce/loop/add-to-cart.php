<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://woo.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

echo apply_filters(
	'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
	sprintf(
		'<a href="%s" data-quantity="%s" class="%s" %s><span class="icon-container">%s</span></a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
		esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
		isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
		// esc_html( $product->add_to_cart_text() ),
		'<svg xmlns="http://www.w3.org/2000/svg" width="22" height="19" viewBox="0 0 22 19" fill="none">
		<path d="M17.1436 1V7.85714" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
		<path d="M13.7148 4.42822H20.572" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
		<path fill-rule="evenodd" clip-rule="evenodd" d="M18.0596 10.4285L17.425 12.6417C17.2845 13.1281 16.99 13.5558 16.5859 13.8608C16.1818 14.1658 15.6896 14.3316 15.1833 14.3333H6.81667C6.31036 14.3316 5.81824 14.1658 5.4141 13.8608C5.00995 13.5558 4.71554 13.1281 4.575 12.6417L2.56667 5.625C2.56667 5.62083 2.56458 5.61458 2.5625 5.60833C2.56042 5.60208 2.55833 5.59583 2.55833 5.59167L1.81667 3H1C0.734783 3 0.48043 2.89464 0.292893 2.70711C0.105357 2.51957 0 2.26522 0 2C0 1.73478 0.105357 1.48043 0.292893 1.29289C0.48043 1.10536 0.734783 1 1 1H2.06667C2.42877 1.00275 2.78044 1.12162 3.06996 1.33913C3.35947 1.55664 3.57155 1.86131 3.675 2.20833L4.275 4.33333H11.1844C11.2622 5.03765 11.4467 5.70982 11.7214 6.33333H4.85L6.49167 12.0917C6.51299 12.1613 6.55603 12.2224 6.6145 12.2658C6.67297 12.3093 6.74381 12.333 6.81667 12.3333H15.1833C15.2562 12.333 15.327 12.3093 15.3855 12.2658C15.444 12.2224 15.487 12.1613 15.5083 12.0917L16.0616 10.151C16.6762 10.3318 17.3266 10.4287 17.9997 10.4287C18.0197 10.4287 18.0396 10.4287 18.0596 10.4285ZM8.05245 18.2593C8.23559 17.9852 8.33333 17.663 8.33333 17.3333C8.33114 16.892 8.15484 16.4693 7.84276 16.1572C7.53067 15.8452 7.10802 15.6689 6.66667 15.6667C6.33703 15.6667 6.0148 15.7644 5.74072 15.9475C5.46663 16.1307 5.25301 16.391 5.12687 16.6955C5.00072 17.0001 4.96772 17.3352 5.03202 17.6585C5.09633 17.9818 5.25507 18.2788 5.48815 18.5118C5.72124 18.7449 6.01821 18.9037 6.34152 18.968C6.66482 19.0323 6.99993 18.9993 7.30447 18.8731C7.60902 18.747 7.86931 18.5334 8.05245 18.2593ZM14.4074 15.9475C14.6815 15.7644 15.0037 15.6667 15.3333 15.6667C15.7747 15.6689 16.1973 15.8452 16.5094 16.1572C16.8215 16.4693 16.9978 16.892 17 17.3333C17 17.663 16.9023 17.9852 16.7191 18.2593C16.536 18.5334 16.2757 18.747 15.9711 18.8731C15.6666 18.9993 15.3315 19.0323 15.0082 18.968C14.6849 18.9037 14.3879 18.7449 14.1548 18.5118C13.9217 18.2788 13.763 17.9818 13.6987 17.6585C13.6344 17.3352 13.6674 17.0001 13.7935 16.6955C13.9197 16.391 14.1333 16.1307 14.4074 15.9475Z" fill="white"></path>
		</svg>'
	),
	$product,
	$args
);
