<?php

/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.3.6
 */

defined('ABSPATH') || exit;

?>
<?php
if (is_cart()) {
?>
	<style>
		.woocommerce-shipping-totals.shipping {
			display: none;
		}

		.coupon.under-proceed {
			display: grid;
			grid-template-columns: 2fr 1fr;
			gap: 1rem;
		}

		#coupon_code {
			border-radius: 8px;
			border: 1px solid var(--e-global-color-d9c2e76, #808080);
			background-color: var(--e-global-color-4d19657);
		}

		.coupon button[name="apply_coupon"] {
			border-radius: 4px;
			background: var(--e-global-color-d85c849, #253D4E) !important;
			border-color: var(--e-global-color-d85c849, #253D4E) !important;
			font-weight: 500;
		}

		.coupon {
			margin-bottom: 4px;
		}

		.elementor-element .elementor-widget-woocommerce-cart .woocommerce .cart_totals table.shop_table_responsive tr td:before,
		.elementor-element .elementor-widget-woocommerce-cart .woocommerce .cart_totals table.shop_table td .woocommerce-Price-amount {
			font-size: 1rem;
			font-weight: 400;
			color: var(--e-global-color-d85c849, #253D4E);
		}

		.elementor-element .elementor-widget-woocommerce-cart .woocommerce .cart_totals table.shop_table_responsive tr.order-total td:before,
		.elementor-element .elementor-widget-woocommerce-cart .woocommerce .cart_totals table.shop_table tr.order-total td .woocommerce-Price-amount {
			font-size: 20px;
			font-weight: 700;
			color: var(--e-global-color-d85c849, #253D4E) !important;
		}

		.elementor-widget-woocommerce-cart .woocommerce .cart_totals h3 {
			font-size: 16px;
			font-weight: 700;
			color: var(--e-global-color-d85c849, #253D4E) !important;
		}

		.elementor-widget-woocommerce-cart .woocommerce .cart_totals h2 {
			line-height: 30px;
			border-bottom: 1px solid rgba(128, 128, 128, 0.5);
			;
		}

		.e-cart__column .woocommerce-cart-form .shop_table th.product-name,
		.e-cart__column .woocommerce-cart-form .shop_table th.product-price,
		.e-cart__column .woocommerce-cart-form .shop_table th.product-price-discounted,
		.e-cart__column .woocommerce-cart-form .shop_table th.product-quantity,
		.e-cart__column .woocommerce-cart-form .shop_table th.product-total {
			border-bottom: 1px solid rgba(128, 128, 128, 0.5);
			;

		}


		.elementor-widget-woocommerce-cart .woocommerce .cart_totals table.shop_table td {
			padding: 12px 0;
		}

		.elementor-element .elementor-widget-woocommerce-cart .e-cart__container {
			grid-template-columns: 2fr 1fr;
		}
	</style>
<?php
}
?>
<div class="cart_totals <?php echo (WC()->customer->has_calculated_shipping()) ? 'calculated_shipping' : ''; ?>">

	<?php do_action('woocommerce_before_cart_totals'); ?>

	<h2><?php esc_html_e('Order Summary', 'woocommerce'); ?></h2>
	<h3><?php esc_html_e('Items in Cart', 'woocommerce'); ?></h3>
	<table cellspacing="0" class="shop_table shop_table_responsive">
		<!-- <form class="woocommerce-coupon-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post"> -->
			<div class="coupon under-proceed">
				<input type="text" class="input-text" id="coupon_code_check" value="" placeholder="<?php esc_attr_e('Enter promo code here', 'woocommerce'); ?>" style="width: 100%" />
				<button type="button"
				class="button btn_coupon" 
				id="apply_coupon_check" style="width: 100%">
				<svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12" fill="none">
						<path d="M6 12C4.325 12 2.90625 11.4187 1.74375 10.2562C0.58125 9.09375 0 7.675 0 6C0 4.325 0.58125 2.90625 1.74375 1.74375C2.90625 0.58125 4.325 0 6 0C6.8625 0 7.6875 0.17825 8.475 0.53475C9.2625 0.89075 9.9375 1.4 10.5 2.0625V0.75C10.5 0.5375 10.572 0.35925 10.716 0.21525C10.8595 0.0717501 11.0375 0 11.25 0C11.4625 0 11.6405 0.0717501 11.784 0.21525C11.928 0.35925 12 0.5375 12 0.75V4.5C12 4.7125 11.928 4.8905 11.784 5.034C11.6405 5.178 11.4625 5.25 11.25 5.25H7.5C7.2875 5.25 7.1095 5.178 6.966 5.034C6.822 4.8905 6.75 4.7125 6.75 4.5C6.75 4.2875 6.822 4.10925 6.966 3.96525C7.1095 3.82175 7.2875 3.75 7.5 3.75H9.9C9.5 3.05 8.95325 2.5 8.25975 2.1C7.56575 1.7 6.8125 1.5 6 1.5C4.75 1.5 3.6875 1.9375 2.8125 2.8125C1.9375 3.6875 1.5 4.75 1.5 6C1.5 7.25 1.9375 8.3125 2.8125 9.1875C3.6875 10.0625 4.75 10.5 6 10.5C6.8625 10.5 7.6595 10.2717 8.391 9.81525C9.122 9.35925 9.66875 8.75 10.0312 7.9875C10.0938 7.85 10.197 7.7345 10.341 7.641C10.4845 7.547 10.6313 7.5 10.7812 7.5C11.0688 7.5 11.2845 7.6 11.4285 7.8C11.572 8 11.5875 8.225 11.475 8.475C11 9.5375 10.2688 10.3905 9.28125 11.034C8.29375 11.678 7.2 12 6 12Z" fill="white" />
					</svg> <?php esc_attr_e('Apply', 'woocommerce'); ?>
				</button>
			</div>
		<!-- </form> -->
		<?php
			$subtotal = 0;

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$product = $cart_item['data'];
				$subtotal += $product->get_regular_price() * $cart_item['quantity'];
			}

			if ($subtotal > 0) {
				echo '<tr><th>Subtotal 1111</th><td data-title="Subtotal">' . wc_price($subtotal) . '</td></tr>';
			}
		?>
		<?php
		$discount_total = 0;
		foreach (WC()->cart->get_cart() as $cart_item_key => $values) {
			$product = $values['data'];
			if ($product->is_on_sale()) {
				$regular_price = $product->get_regular_price();
				$sale_price = $product->get_sale_price();
				$discount = ((float)$regular_price - (float)$sale_price) * (int)$values['quantity'];
				$discount_total += $discount;
			}
		}
		if ($discount_total > 0) {
			echo '<tr><th>Discount</th><td data-title="Discount">- ' . wc_price($discount_total + WC()->cart->get_discount_total()) . '</td></tr>';
		}
		?>
		<?php foreach (WC()->cart->get_coupons() as $code => $coupon) : ?>
			<tr class="cart-discount coupon-<?php echo esc_attr(sanitize_title($code)); ?>">
				<th>Promotion Code Applied</th>
				<td data-title="Promotion Code Applied"><?php wc_cart_totals_coupon_html($coupon); 
				?></td>
			</tr>
		<?php endforeach; ?>

		<!-- <?php if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) : ?>

			<?php do_action('woocommerce_cart_totals_before_shipping'); ?>

			<?php wc_cart_totals_shipping_html(); ?>

			<?php do_action('woocommerce_cart_totals_after_shipping'); ?>

		<?php elseif (WC()->cart->needs_shipping() && 'yes' === get_option('woocommerce_enable_shipping_calc')) : ?>

			<tr class="shipping">
				<th><?php esc_html_e('Shipping', 'woocommerce'); ?></th>
				<td data-title="<?php esc_attr_e('Shipping', 'woocommerce'); ?>"><?php woocommerce_shipping_calculator(); ?></td>
			</tr>

		<?php endif; ?> -->

		<?php foreach (WC()->cart->get_fees() as $fee) : ?>
			<tr class="fee">
				<th><?php echo esc_html($fee->name); ?></th>
				<td data-title="<?php echo esc_attr($fee->name); ?>"><?php wc_cart_totals_fee_html($fee); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php
		if (wc_tax_enabled() && !WC()->cart->display_prices_including_tax()) {
			$taxable_address = WC()->customer->get_taxable_address();
			$estimated_text  = '';

			if (WC()->customer->is_customer_outside_base() && !WC()->customer->has_calculated_shipping()) {
				/* translators: %s location. */
				$estimated_text = sprintf(' <small>' . esc_html__('(estimated for %s)', 'woocommerce') . '</small>', WC()->countries->estimated_for_prefix($taxable_address[0]) . WC()->countries->countries[$taxable_address[0]]);
			}

			if ('itemized' === get_option('woocommerce_tax_total_display')) {
				foreach (WC()->cart->get_tax_totals() as $code => $tax) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
		?>
					<tr class="tax-rate tax-rate-<?php echo esc_attr(sanitize_title($code)); ?>">
						<th><?php echo esc_html($tax->label) . $estimated_text; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped 
							?></th>
						<td data-title="<?php echo esc_attr($tax->label); ?>"><?php echo wp_kses_post($tax->formatted_amount); ?></td>
					</tr>
				<?php
				}
			} else {
				?>
				<tr class="tax-total">
					<th><?php echo esc_html(WC()->countries->tax_or_vat()) . $estimated_text; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped 
						?></th>
					<td data-title="GST 8%"><?php wc_cart_totals_taxes_total_html(); ?></td>
				</tr>
		<?php
			}
		}
		?>

		<?php do_action('woocommerce_cart_totals_before_order_total'); ?>

		<tr class="order-total">
			<th><?php esc_html_e('Total', 'woocommerce'); ?></th>
			<td data-title="<?php esc_attr_e('Sub-Total', 'woocommerce'); ?>"><span style="    float: left; margin-left: 10px;">Incl. GST</span> <?php wc_cart_totals_order_total_html(); ?></td>
		</tr>

		<?php do_action('woocommerce_cart_totals_after_order_total'); ?>

	</table>

	<div class="wc-proceed-to-checkout">
		<?php
		if (is_user_logged_in()) {
			do_action('woocommerce_proceed_to_checkout');
		} else {
			echo '<a href="' . home_url() . '/guest-checkout/" class="checkout-button button alt wc-forward">Proceed to Checkout</a>';
		}
		?>

	</div>

	<?php do_action('woocommerce_after_cart_totals'); ?>

</div>