<?php

/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_cart'); ?>
<style>
	button.service-option,
	button.service-option:hover,
	button.service-option:focus button.service-option:focus-visible {
		width: 100%;
		padding: 10px;
		background: var(--e-global-color-814804a) !important;
		color: var(--e-global-color-d85c849) !important;
		text-align: left;
		font-weight: 500;
		box-shadow: none;
	}

	.woocommerce-cart-form .e-shop-table.e-cart-section {
		margin: 0;
	}

	.option-item {
		display: flex;
		align-items: center;
		margin: 10px 0;
		gap: 1rem;
	}

	.dropdown-menu {
		padding: 0 20px;
		margin-top: 10px;
	}

	.dropdown {
		border-top: 1px dashed;
		margin-top: 10px;
		padding-top: 10px;
	}

	.dropdown-menu ul {
		padding: 0;
	}

	.elementor-widget-woocommerce-cart .woocommerce {
		font-family: var(--e-global-typography-primary-font-family);
	}

	.elementor-widget-woocommerce-cart div.e-cart__container {
		gap: 5rem;
	}

	.quantity,
	.woocommerce .quantity input[type="number"].qty {
		width: 100%;
	}

	.button-link {
		color: var(--e-global-color-d85c849) !important;
		padding: 0 !important;
		background: none !important;
		box-shadow: none !important;
		text-decoration: underline !important;
		font-size: 16px !important;
		font-weight: 400 !important;
	}

	.elementor-widget-woocommerce-cart .woocommerce .remove.button-link {
		color: var(--e-global-color-d85c849) !important;
		font-size: 12px !important;
		margin-top: 10px;
	}

	th.product-name {
		color: var(--e-global-color-d85c849);
		font-size: 20px !important;
		font-weight: 700;
		line-height: 25px;
		letter-spacing: 0.2px;
	}

	.product-price,
	.product-price-discounted,
	.product-quantity,
	.product-total {
		font-weight: 500 !important;
	}

	.product-price,
	.product-price-discounted,
	.product-quantity,
	.product-total,
	.product-name, .cart_totals h2 {
		color: var(--e-global-color-d85c849) !important;
	}

	.woocommerce-cart-form__cart-item.cart_item>td {
		vertical-align: top !important;
	}

	form.woocommerce-cart-form td:not(.product-name),
	form.woocommerce-cart-form th:not(.product-name) {
		text-align: center;
	}

	.elementor-widget-woocommerce-cart .e-cart-section {
		border: none;
		padding: 0;
	}

	.elementor-widget-woocommerce-cart .woocommerce a:not(.add_to_cart_button):not(.restore-item):not(.wc-backward):not(.wc-forward),
	.elementor-widget-woocommerce-cart .woocommerce a:not(.add_to_cart_button):not(.restore-item):not(.wc-backward):not(.wc-forward):hover {
		color: var(--e-global-color-d85c849);
		font-weight: 700;
	}

	.elementor-widget-woocommerce-cart .woocommerce .cart_totals h2 {
		font-size: 20px;
	}
</style>
<form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
	<?php do_action('woocommerce_before_cart_table'); ?>

	<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
		<thead>
			<tr>
				<th class="product-name" colspan="2" width="40%"><?php esc_html_e('Product', 'woocommerce'); ?></th>
				<th class="product-price" width="15%"><?php esc_html_e('Price', 'woocommerce'); ?></th>
				<th class="product-price-discounted" width="15%"><?php esc_html_e('Discounted', 'woocommerce'); ?></th>
				<th class="product-quantity" width="15%"><?php esc_html_e('Quantity', 'woocommerce'); ?></th>
				<th class="product-total" width="15%"><?php esc_html_e('Total', 'woocommerce'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php do_action('woocommerce_before_cart_contents'); ?>

			<?php
			foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
				$_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
				$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

				if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
					$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
			?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">
						<td class="product-thumbnail">
							<?php
							$thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

							if (!$product_permalink) {
								echo $thumbnail; // PHPCS: XSS ok.
							} else {
								printf('<a href="%s">%s</a>', esc_url($product_permalink), $thumbnail); // PHPCS: XSS ok.
							}
							?>
						</td>

						<td class="product-name" data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
							<?php
							if (!$product_permalink) {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
							} else {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
							}

							do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

							// Meta data.
							echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

							// Backorder notification.
							if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
							}
							$product_short_description = $_product->get_short_description();
							if ($product_short_description) {
								echo wp_kses_post('<p class="product-short-description">' . $product_short_description . '</p>');
							}
							echo '<div class="dropdown">';
							echo '<button class="btn btn-secondary dropdown-toggle service-option" type="button" id="job_type_filter_button' . $_product->get_id() . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
							echo 'Service';
							echo '</button>';
							echo '<div class="dropdown-menu" aria-labelledby="job_type_filter_button' . $_product->get_id() . '" id="job_type_filter_menu' . $_product->get_id() . '">';
							echo '<ul>';
							echo '<li class="option-item"><i class="far fa-circle"></i><a href="#">None $0.00</a></li>';
							echo '<li class="option-item"><i class="far fa-circle"></i><a href="#">Standard Installation $0.00</a></li>';
							echo '</ul>';
							echo '</div>';
							?>
							<script>
								jQuery(document).ready(function($) {
									$('#job_type_filter_button<?= $_product->get_id() ?>').click(function() {
										$('#job_type_filter_menu<?= $_product->get_id() ?>').toggleClass('show');
									});
								});
							</script>
						</td>

						<td class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
						<?php
							$product = wc_get_product($_product); 
							$regular_price = $product->get_regular_price();
							echo apply_filters('woocommerce_cart_item_price', wc_price($regular_price), $cart_item, $cart_item_key); // PHPCS: XSS ok.
						?>
						</td>
						<td class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
							<?php
							// Regular price
							$regular_price = $_product->get_regular_price();
							// Current price
							$current_price = $_product->get_price();
							// Discount amount for this item
							$discount_amount = $regular_price - $current_price;
							// Add the discount amount to the total discount

							echo apply_filters('woocommerce_cart_item_price', wc_price($discount_amount), $cart_item, $cart_item_key); // PHPCS: XSS ok.
							?>
						</td>
						<td class="product-quantity" data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>">
							<?php
							if ($_product->is_sold_individually()) {
								$product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
							} else {
								$product_quantity = woocommerce_quantity_input(
									array(
										'input_name'   => "cart[{$cart_item_key}][qty]",
										'input_value'  => $cart_item['quantity'],
										'max_value'    => $_product->get_max_purchase_quantity(),
										'min_value'    => '0',
										'product_name' => $_product->get_name(),
									),
									$_product,
									false
								);
							}

							echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item); // PHPCS: XSS ok.
							?>
							<?php
							echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								'woocommerce_cart_item_remove_link',
								sprintf(
									'<a href="%s" class="remove button-link" aria-label="%s" data-product_id="%s" data-product_sku="%s">Remove</a>',
									esc_url(wc_get_cart_remove_url($cart_item_key)),
									esc_html__('Remove this item', 'woocommerce'),
									esc_attr($product_id),
									esc_attr($_product->get_sku())
								),
								$cart_item_key
							);
							?>
						</td>

						<td class="product-subtotal" data-title="<?php esc_attr_e('Subtotal', 'woocommerce'); ?>">
							<?php
							echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
							?>
						</td>
					</tr>
			<?php
				}
			}
			?>

			<?php do_action('woocommerce_cart_contents'); ?>

			<tr>
				<td colspan="6" class="actions">

					
						<div class="coupon">
							<input type="hidden" name="coupon_code" class="input-text" id="coupon_code" value=""/> 
							<button type="submit"
							id="apply_coupon"
							class="button<?php echo esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ); ?>" 
							name="apply_coupon" 
							style="display:none !important;">
								<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>
							</button>
							<?php do_action('woocommerce_cart_coupon'); ?>
						</div>
				

					<div style="display: flex; flex-direction: row-reverse; align-items: center; gap: 1rem; flex-wrap: nowrap;">
						<!-- <button type="submit" class="button-link" name="update_cart" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Refresh Cart', 'woocommerce'); ?></button> -->
						<button type="submit" class="button<?php echo esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ); ?>" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Refresh Cart', 'woocommerce' ); ?></button>
						<a href="<?php echo get_home_url(); ?>/cart/?empty-cart" class="button-link"><?php esc_html_e('Clear Cart', 'woocommerce'); ?></a>
						<div style="margin-right: auto;">for Bulk orders, click <a class="button-link" href="<?php echo get_home_url(); ?>/bulk-orders">here</a> to contact us directly. </div>
					</div>
					<?php do_action('woocommerce_cart_actions'); ?>

					<?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
				</td>
			</tr>

			<?php do_action('woocommerce_after_cart_contents'); ?>
		</tbody>
	</table>
	<?php do_action('woocommerce_after_cart_table'); ?>
</form>
<!-- <script>
	$(document).ready(function() {
		$('.woocommerce-cart-form .e-clear').remove();
		$('.woocommerce-cart-form .coupon.e-cart-section.shop_table').remove();
		$('.elementor-widget-woocommerce-cart .woocommerce .cart_totals h2').text('Order Summary');
	})
</script> -->
<?php do_action('woocommerce_before_cart_collaterals'); ?>
</script>
<div class="cart-collaterals">
	<?php
	/**
	 * Cart collaterals hook.
	 *
	 * @hooked woocommerce_cross_sell_display
	 * @hooked woocommerce_cart_totals - 10
	 */
	do_action('woocommerce_cart_collaterals');
	?>
</div>

<?php do_action('woocommerce_after_cart'); ?>