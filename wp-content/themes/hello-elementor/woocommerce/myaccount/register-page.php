<?php
/**
 * Register Form
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

?>

<?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>
<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
<div class="div-wrap-form-register form-register-area">
	<div class="col-2 col-form-register">
		<h2 style="font-size: 30px;"><?php esc_html_e( 'Sign Up', 'woocommerce' ); ?></h2>
		<p>Already have an account?<a style="color:#1d78d0;" href="<?php echo home_url() ?>/my-account"> Log In</a> now.</p>
        <form method="post"  enctype="multipart/form-data" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

            <?php do_action( 'woocommerce_register_form_start' ); ?>

            <div class="woocommerce-form-row row-first-last-name woocommerce-form-row--wide form-row form-row-wide">
                <div class="input-wrapper">
                    <label for="first_name"><?php _e('First Name', 'text-domain'); ?><span class="required">*</span></label>
                    <input type="text" class="input-text" name="billing_first_name" id="billing_first_name" value="<?php if (isset($_POST['billing_first_name'])) echo esc_attr($_POST['billing_first_name']); ?>" required/>
                </div>
                <div class="input-wrapper">
                    <label for="last_name"><?php _e('Last Name', 'text-domain'); ?><span class="required">*</span></label>
                    <input type="text" class="input-text" name="billing_last_name" id="billing_last_name" value="<?php if (isset($_POST['billing_last_name'])) echo esc_attr($_POST['billing_last_name']); ?>" required/>
                </div>
            </div>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                </p>

            <?php endif; ?>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" placeholder="e.g. John@gmail.com"/><?php // @codingStandardsIgnoreLine ?>
            </p>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" placeholder="Please enter 8 or more characters with special characters"/>
                </p>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="password_2"><?php _e( 'Confirm password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="confirm_password" id="confirm_password" autocomplete="new-password"/>
                </p>
            <?php else : ?>

                <p style="font-size: 14px; margin-left: 4px;"><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

            <?php endif; ?>

            <?php do_action( 'woocommerce_register_form' ); ?>

            <p class="woocommerce-form-row form-row submit-btn-reg">
                <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                <button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Sign Up', 'woocommerce' ); ?></button>
    <!--             <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="woocommerce-button button woocommerce-form-login" name="login"><?php esc_attr_e( 'Log in', 'woocommerce' ); ?></a> -->
            </p>
            <?php do_action( 'woocommerce_register_form_end' ); ?>

        </form>
	</div>
</div>
<?php endif; ?>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function () {
	var textOnlyInput = document.getElementById('billing_first_name');
	var textOnlyInputl = document.getElementById('billing_last_name');

	textOnlyInput.addEventListener('input', function () {
		var inputValue = this.value;

		var textValue = inputValue.replace(/[0-9]/g, '');

		this.value = textValue;
	});
	textOnlyInputl.addEventListener('input', function () {
		var inputValue = this.value;
		var textValue = inputValue.replace(/[0-9]/g, '');
		this.value = textValue;
	});
});
</script>
<style>
.form-register-area{
	display: flex;
	justify-content: center;
}
.col-form-register{
	width: 100%;
	max-width: 600px;
	padding: 30px;
}

</style>