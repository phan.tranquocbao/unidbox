<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_lost_password_form' );
?>
<style>
    div[data-id="a53f573"] {
        padding: 10px;
    }
</style>
<div class="div-wrap-form-login form-lost-area">
    <div class="col-form-lost" id="customer_lost_pass">
        <form method="post" class="woocommerce-ResetPassword lost_reset_password">
            
            <p><?php echo apply_filters( 'woocommerce_lost_password_message', esc_html__( 'Lost your password? Please enter your email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?></p>
            <?php // @codingStandardsIgnoreLine ?>
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                <label for="user_login"><?php esc_html_e( 'Email Address*', 'woocommerce' ); ?></label>
                <input class="woocommerce-Input woocommerce-Input--text input-text" placeholder="Enter your Email Address" required type="text" name="user_login" id="user_login" />
            </p>
    
            <div class="clear"></div>
            <script src="https://www.google.com/recaptcha/api.js?render=6LeO1lAnAAAAAKQk8sj6bJVc8oCVh1OsSnHWvIj4"></script>
            <script>
                grecaptcha.ready(function () {
                    grecaptcha.execute('6LeO1lAnAAAAAKQk8sj6bJVc8oCVh1OsSnHWvIj4', {action: 'register'}).then(function (token) {
                        document.getElementById('g-recaptcha-response').value = token;
                    });
                });
            </script>
            <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
            <?php do_action( 'woocommerce_lostpassword_form' ); ?>
        
            <p class="woocommerce-form-row form-row">
                <input type="hidden" name="wc_reset_password" value="true" />
                <button type="submit" class="elementor-button button" value="<?php esc_attr_e( 'Reset Password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset password', 'woocommerce' ); ?></button>
            </p>
            <?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>
        </form>
    </div>
</div>
<script type="text/javascript">
    // Code here...
</script>
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
<style>
.form-lost-area{
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 40vh;
}
.col-form-lost{
    width: 100%;
    max-width: 600px;
    padding: 30px;
    background-color: #fff;
    border-radius: 15px;
    box-shadow: -2px 4px 20px 0px #00000026;
}
.col-form-lost .woocommerce-form-row{
    width: 100% !important;
}
.col-form-lost h2.title-form-login{
    text-align: center;
}
.col-form-lost form input{
    width: 100% !important;
    border: 1px solid #C4C4C4;
}
form.woocommerce-ResetPassword label{
    margin-bottom: 10px;
}
.col-form-lost form.woocommerce-ResetPassword button.button{
    float: none !important;
    width: 100%;
    font-size: 16px;
    margin-bottom: 20px;
}
</style>
<?php
do_action( 'woocommerce_after_lost_password_form' );
