<?php
/**
 * Login Form
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
?>
<?php
do_action( 'woocommerce_before_customer_login_form' ); ?>
<div class="div-wrap-form-login form-login-area">
	<div class="col-form-login" id="customer_login">
			<h2 class="title-form-login" style="font-size: 30px;"><?php esc_html_e( 'Sign In', 'woocommerce' ); ?></h2>
			<form class="woocommerce-form woocommerce-form-login login" method="post">
				<?php do_action( 'woocommerce_login_form_start' ); ?>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="username"><?php esc_html_e( 'Email Address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="Enter your email address" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
				</p>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide pw-row">
					<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
					<input class="woocommerce-Input woocommerce-Input--text input-text" placeholder="Enter your Password" type="password" name="password" id="password" autocomplete="current-password" />
				</p>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide pw-row">
					<!-- <span><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Forgot Password?', 'woocommerce' ); ?></a></span> -->
					<span><a href="<?php echo home_url(); ?>/lost-password"><?php esc_html_e( 'Forgot Password?', 'woocommerce' ); ?></a></span>
				</p>
				<?php do_action( 'woocommerce_login_form' ); ?>

				<p class="form-row">
					<!-- <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
						<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
					</label> -->
					<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
					<div class="submit-losspw">
						<button type="submit" class="elementor-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Sign In', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
						<p>Do not have an account yet? <a href="<?php echo home_url(); ?>/register">Sign Up</a></p>
					</div>
					<?php do_action( 'woocommerce_login_form_end' ); ?>
					<!-- <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>?action=register" class="woocommerce-button button woocommerce-form-register" name="register"><?php esc_attr_e( 'Register', 'woocommerce' ); ?></a> -->	
				</p>
				<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide pw-row">
					
				</div>
			</form>
	</div>
</div>
<script type="text/javascript">
	// Code here...
</script>
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
<style>
.form-login-area{
	display: flex;
	justify-content: center;
	align-items: center;
	min-height: 84vh;
	background-image: url("/tgb_unidbox/wp-content/uploads/2024/01/Pexels-Photo-by-Vecislavas-Popa-1.png");
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
}
.col-form-login{
	width: 100%;
	max-width: 600px;
	padding: 30px;
	background-color: #fff;
	border-radius: 15px;
}
.col-form-login h2.title-form-login{
	text-align: center;
}
.col-form-login form .woocommerce-form-login__submit{
	margin-bottom: 20px;
}
.col-form-login form input{
	border: 1px solid #C4C4C4;
}
.col-form-login .woocommerce-form-login .woocommerce-form-login__submit{
	float: none !important;
	width: 100%;
	font-size: 16px;
}
</style>