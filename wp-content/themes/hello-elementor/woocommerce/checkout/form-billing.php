<?php

/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined('ABSPATH') || exit;
?>
<?php
if (is_checkout()) {
?>
	<style>
		#shipping_method>li {
			padding: 16px;
			border: 1px solid black;
		}
		#shipping_method>li.active {

		background: linear-gradient(0deg, rgba(254, 168, 47, 0.20) 0%, rgba(254, 168, 47, 0.20) 100%), #FFF;

	}

		#shipping_method>li input[type='radio'] {
			display: none;
		}
	</style>
	<script>
		$(document).ready(function() {
			changeInputMethod();
		});

		function changeInputMethod(){
			var radioButtons = document.querySelectorAll('#shipping_method input[type="radio"]');
			for (var i = 0; i < radioButtons.length; i++) {
				var label = radioButtons[i];
				var li = radioButtons[i].closest('li');
				var text = li.querySelector('label');
				if (radioButtons[i].checked) {
					label.checked = true;
					label.classList.add('active');
					text.classList.add('active');
					li.classList.add('active');
					text.insertAdjacentHTML('afterbegin', '<i class="fas fa-dot-circle icon"></i> ');
				} else {
					label.classList.remove('active');
					text.classList.remove('active');
					li.classList.remove('active');
					text.insertAdjacentHTML('afterbegin', '<i class="far fa-circle icon"></i> ');
				}
				radioButtons[i].addEventListener('change', function() {
					$('#shipping_method .icon').remove();
					changeInputMethod();
				});
			}
		}
	</script>
<?php
}
?>
<?php if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) : ?>

	<?php do_action('woocommerce_cart_totals_before_shipping'); ?>

	<?php wc_cart_totals_shipping_html(); ?>

	<?php do_action('woocommerce_cart_totals_after_shipping'); ?>

<?php elseif (WC()->cart->needs_shipping() && 'yes' === get_option('woocommerce_enable_shipping_calc')) : ?>

	<tr class="shipping">
		<th><?php esc_html_e('Shipping Method', 'woocommerce'); ?></th>
		<td data-title="<?php esc_attr_e('Shipping Method', 'woocommerce'); ?>"><?php woocommerce_shipping_calculator(); ?></td>
	</tr>

<?php endif; ?>
<div class="woocommerce-additional-fields">
	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

		<?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

			<h3><?php esc_html_e( 'Additional information', 'woocommerce' ); ?></h3>

		<?php endif; ?>

		<div class="woocommerce-additional-fields__field-wrapper">
			<?php foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
				<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
			<?php endforeach; ?>
		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>
<div class="woocommerce-billing-fields">
	<?php if (wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>

		<h3><?php esc_html_e('Billing &amp; Shipping', 'woocommerce'); ?></h3>

	<?php else : ?>

		<h3><?php esc_html_e('Billing details', 'woocommerce'); ?></h3>

	<?php endif; ?>

	<?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>

	<div class="woocommerce-billing-fields__field-wrapper">
		<?php
		$fields = $checkout->get_checkout_fields('billing');

		foreach ($fields as $key => $field) {
			woocommerce_form_field($key, $field, $checkout->get_value($key));
		}
		?>
	</div>

	<?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>

	<h3 id="ship-same-billing">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
				<input id="ship-same-billing-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" type="checkbox" checked name="ship_to_different_address" value="1"> <span>My billing and shipping address are the same</span>
			</label>
		</h3>
		<script>
			$(document).ready(function(){
				$('#ship-to-different-address').hide();
				$('div.shipping_address' ).show();
				$('#ship-same-billing-checkbox').change(function() {
					
					$('#ship-same-billing-checkbox').is(":checked") ? $( 'div.shipping_address' ).slideUp() : $( 'div.shipping_address' ).slideDown();

				});
			});
		</script>
</div>

<?php if (!is_user_logged_in() && $checkout->is_registration_enabled()) : ?>
	<div class="woocommerce-account-fields">
		<?php if (!$checkout->is_registration_required()) : ?>

			<p class="form-row form-row-wide create-account">
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked((true === $checkout->get_value('createaccount') || (true === apply_filters('woocommerce_create_account_default_checked', false))), true); ?> type="checkbox" name="createaccount" value="1" /> <span><?php esc_html_e('Create an account?', 'woocommerce'); ?></span>
				</label>
			</p>

		<?php endif; ?>

		<?php do_action('woocommerce_before_checkout_registration_form', $checkout); ?>

		<?php if ($checkout->get_checkout_fields('account')) : ?>

			<div class="create-account">
				<?php foreach ($checkout->get_checkout_fields('account') as $key => $field) : ?>
					<?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>

		<?php endif; ?>

		<?php do_action('woocommerce_after_checkout_registration_form', $checkout); ?>
	</div>
<?php endif; ?>