<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.3
 */

defined( 'ABSPATH' ) || exit;

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
?>
<style>
	#payment li input[type='radio'] {
		display: none;
	}
	input[type=checkbox] {
		accent-color: var(--e-global-color-accent);
	}
	label { 
		display: flex; 
		align-items: center; 
	} 
	input[type=checkbox]{ 
		flex: none; 
	} 
</style>
<div id="payment" class="woocommerce-checkout-payment">
	<?php if ( WC()->cart->needs_payment() ) : ?>
		<h3>Payment Methods</h3>
		
		<script>
			$(document).ready(function() {
			changePaymentMethod();
		});

		function changePaymentMethod(){
			var radioButtons = document.querySelectorAll('#payment input[type="radio"]');
			for (var i = 0; i < radioButtons.length; i++) {
				var label = radioButtons[i];
				var li = radioButtons[i].closest('li');
				var text = li.querySelector('label');
				if (radioButtons[i].checked) {
					label.checked = true;
					label.classList.add('active');
					text.classList.add('active');
					li.classList.add('active');
					text.insertAdjacentHTML('afterbegin', '<i class="fas fa-dot-circle icon"></i> ');
				} else {
					label.classList.remove('active');
					text.classList.remove('active');
					li.classList.remove('active');
					text.insertAdjacentHTML('afterbegin', '<i class="far fa-circle icon"></i> ');
				}
				radioButtons[i].addEventListener('change', function() {
					$('#payment .icon').remove();
					changePaymentMethod();
				});
			}
		}
		</script>
		<ul class="wc_payment_methods payment_methods methods">
			<?php
			if ( ! empty( $available_gateways ) ) {
				foreach ( $available_gateways as $gateway ) {
					wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
				}
			} else {
				echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
			}
			?>
		</ul>
	<?php endif; ?>
	<div style="display: flex; margin-top: 16px; gap: 1rem; align-items: center">
	<svg xmlns="http://www.w3.org/2000/svg" width="30" height="28" viewBox="0 0 30 28" fill="none">
	<path d="M19.7334 13.25H18.6299V9.75C18.6299 8.64531 17.7 7.75 16.5527 7.75H12.5281C11.3808 7.75 10.4509 8.64531 10.4509 9.75V13.25H9.34742C9.06019 13.25 8.82812 13.4734 8.82812 13.75V19.75C8.82812 20.0266 9.06019 20.25 9.34742 20.25H19.7334C20.0206 20.25 20.2527 20.0266 20.2527 19.75V13.75C20.2527 13.4734 20.0206 13.25 19.7334 13.25ZM14.9948 16.9531V17.7812C14.9948 17.85 14.9364 17.9062 14.865 17.9062H14.2158C14.1444 17.9062 14.086 17.85 14.086 17.7812V16.9531C13.9521 16.8605 13.8521 16.7294 13.8005 16.5786C13.7488 16.4278 13.7482 16.2651 13.7988 16.114C13.8493 15.9628 13.9483 15.831 14.0816 15.7375C14.2148 15.644 14.3755 15.5936 14.5404 15.5936C14.7053 15.5936 14.866 15.644 14.9992 15.7375C15.1325 15.831 15.2315 15.9628 15.2821 16.114C15.3326 16.2651 15.332 16.4278 15.2804 16.5786C15.2287 16.7294 15.1287 16.8605 14.9948 16.9531ZM17.4615 13.25H11.6194V9.75C11.6194 9.26719 12.0267 8.875 12.5281 8.875H16.5527C17.0541 8.875 17.4615 9.26719 17.4615 9.75V13.25Z" fill="#12A78A"/>
	<path d="M28.5807 14C28.5807 21.4382 22.3126 27.5 14.5403 27.5C6.76806 27.5 0.5 21.4382 0.5 14C0.5 6.56183 6.76806 0.5 14.5403 0.5C22.3126 0.5 28.5807 6.56183 28.5807 14Z" stroke="#12A78A"/>
	</svg>
		<div>We protect your payment information using encryption to provide bank-level security.</div>
		
	</div>
	
	<fieldset>
		<p class="form-row woocommerce-SavedPaymentMethods-saveNew woocommerce-validated" style="">
			<label for="checkbox-accept" style="display:inline;">
				<input id="checkbox-accept" name="checkbox-accept" type="checkbox" value="true" style="width:auto;">
				Please check to acknowledge our <a href="#" target="_blank" rel="noopener noreferrer">Privacy & Terms Policy</a>
			</label>
		</p>
	</fieldset>
	<script>
		$(document).ready(function(){
			$('#place_order').prop('disabled', true);
			$('#checkbox-accept').change(function(){
				$('#place_order').prop('disabled', !this.checked);
			});
		});
	</script>
	<div class="form-row place-order">
		<noscript>
			<?php
			/* translators: $1 and $2 opening and closing emphasis tags respectively */
			printf( esc_html__( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the %1$sUpdate Totals%2$s button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ), '<em>', '</em>' );
			?>
			<br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
		</noscript>

		<?php wc_get_template( 'checkout/terms.php' ); ?>

		<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

		<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

		<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

		<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
	</div>
</div>
<?php
if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_after_payment' );
}
