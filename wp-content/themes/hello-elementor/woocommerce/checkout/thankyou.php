<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>
	<div class="elementor-element elementor-element-14298ac e-con-full e-flex e-con e-child" data-id="14298ac" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;container_type&quot;:&quot;flex&quot;}">
		<div class="elementor-element elementor-element-92ca229 border-bottom-custom elementor-widget elementor-widget-heading" data-id="92ca229" data-element_type="widget" data-widget_type="heading.default">
			<div class="elementor-widget-container">
				<h2 class="elementor-heading-title elementor-size-default">Order Confirmation</h2>		
			</div>
		</div>
		<div class="elementor-element elementor-element-b316c86 elementor-widget elementor-widget-text-editor" data-id="b316c86" data-element_type="widget" data-widget_type="text-editor.default">
			<div class="elementor-widget-container">
				<div>Cart —&nbsp; Checkout / Payment<span style="color: var(--e-global-color-d85c849);">
					<strong> —&nbsp; Complete!</strong></span>
				</div>						
			</div>
		</div>
	</div>
		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>
			<div class="order_thankyou">
				<div class="order_summary_wrap">
					<h4 class="title_order_summary">Order Summary</h4>
					<p class="text text-email-to"> An email confirmation has been sent to: </p>
				</div>
				<div class="summary_order">
					<ul class="summary_order_items">
						<li class="summary_order_item">
							<p> <?php echo $order->get_billing_first_name() .' '. $order->get_billing_last_name(); ?> </p>
							<p> <?php echo $order->get_billing_email() ?></p>
						</li>
						<li class="summary_order_item">
							<p> Contact Number:</p>
							<p><?php echo $order->get_billing_phone(); ?></p>
						</li>
						<hr>
						<li class="summary_order_item">
							<p>Order Number:</p>
							<p>#<?php echo $order->get_id(); ?></p>
						</li>
						<li class="summary_order_item">
							<p> Order Total:</p>
							<p><?php echo wc_price($order->get_total()); ?></p>
						</li>
						<hr>
						<li class="summary_order_item">
							<p>Delivery Method:</p>
							<?php 
								$order_id = absint( get_query_var( 'order-received' ) );
								if ( $order_id ) {
									$order = wc_get_order( $order_id );

									if ( is_a( $order, 'WC_Order' ) && ( $order->has_status( 'processing' ) || $order->has_status( 'on-hold' ) ) ) {
										$shipping_items = $order->get_items( 'shipping' );
										if ( ! empty( $shipping_items ) ) {
											$shipping_method_title = reset( $shipping_items )->get_method_title();
											echo '<p>' . esc_html( $shipping_method_title ) . '</p>';
										}else{
											echo '<p> N/A </p>';
										}
									}
								}
							?>
						</li>
						<!-- <li class="summary_order_item">
							<p>Estimated Delivery:</p>
							<p></p>
						</li> -->
						<hr>
						<li class="summary_order_item">
							<p>Delivery to:</p>
							<p><?php echo $order->get_shipping_first_name() .' '.   $order->get_shipping_last_name()?></p>
						</li>
						<li class="summary_order_item">
							<p>Shipping Address:</p>
							<p style="text-align: right;"><?php echo $order->get_shipping_address_1() .', '.   $order->get_shipping_city() .'<br>'.  $order->get_shipping_country().'<br>'. $order->get_shipping_postcode() ;?></p>
						</li>
						<hr>
					</ul>
				</div>
				<div class="order_details">
					<table class="desktop-view view-large">
						<thead>
							<tr>
								<th style="width: 15%;"><h4>Order Details</h4></th>
								<th></th>
								<th style="width: 10%;">Price</th>
								<th style="width: 10%;">Discounted</th>
								<th style="width: 10%;">Quantity</th>
								<th style="width: 10%;">Total</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if ($order) {
							$items = $order->get_items();
							foreach ($items as $item) {
								$product = $item->get_product();
								$product_image_id = $product->get_image_id();
								$product_image_url = wp_get_attachment_url($product_image_id);
							?>
									<tr>
										<td style="text-align: center;"><img class="img-product-item" src="<?php echo $product_image_url; ?>" alt="<?php echo $product->get_title(); ?>" width="50" height="50"></td>
										<td>
											<p class="image_product_title"><?php echo $product->get_title(); ?></p>
											<p><?php echo $product->get_short_description(); ?></p>
											<hr style="border-top: 1px dashed;">
											<p>Standard Installation $0.00</p>
										</td>
										<td><?php echo wc_price($product->get_regular_price()); ?></td>
										<td><?php echo wc_price(floatval($product->get_regular_price()) - floatval($product->get_price())); ?></td>
										<td><?php echo $item->get_quantity(); ?></td>
										<td><?php echo wc_price($item->get_total()); ?></td>
									</tr>
							<?php
							}
						}
						?>
						</tbody>
					</table>
					<div class="moblie_view view_small">
						<?php if ($order) {
								$items = $order->get_items();
								foreach ($items as $item) {
									$product = $item->get_product();
									$product_image_id = $product->get_image_id();
									$product_image_url = wp_get_attachment_url($product_image_id);
									?>
									<div class="wcspc-item">
										<div class="wcspc-item-inner">
											<div class="wcspc-item-thumb">
												<a href="#">
													<img width="645" height="551" src="<?php echo $product_image_url; ?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" decoding="async">
												</a>
											</div>
											<div class="wcspc-item-info">
												<p class="wcspc-item-title"><?php echo $product->get_title(); ?></p>
												<p class="short-des-item"><?php echo $product->get_short_description(); ?></p>
												<p class="wcspc-item-sku"><?php echo $product->get_sku(); ?></p>
												<p class="wcspc-item-install"> With Installation </p>
												<div class="wcspc-item-price"><?php echo wc_price($item->get_total()); ?></div>
												<p class="wcspc-item-price"><?php echo wc_price($item->get_total()); ?></p>
											</div>
											<p class="wcspc-item-qty">Quantity: <?php echo $item->get_quantity(); ?></p>
										</div>
									</div>
								<?php }
							}
						?>
					</div>
					<div class="order_note">
						<p>Order Notes:</p>
						<p><?php echo $order->get_customer_note() ?: 'N/A'; ?></p>
					</div>
					<hr>
				</div>
				<div class="payment_details">
					<div class="payment_details_title">
						<h4>Payment Details</h4>
					</div>
					<hr>
					<ul class="payment_details_items">
						<li class="payment_details_item">
							<p>Payment Method:</p>
							<p><?php echo $order->get_payment_method_title(); ?></p>
						</li>
						<li class="payment_details_item">
							<p>Subtotal:</p>
							<?php
							$subtotal = 0;
							$discountTotal = 0;

							foreach ($order->get_items() as $item_id => $item) {
								$product = $item->get_product();

								$subtotal += $product->get_regular_price() * $item->get_quantity();
								$item_discount = $product->get_regular_price() - $product->get_price();
								$discountTotal += $item_discount * $item->get_quantity();
							}
							
							?>
							<p><?php echo wc_price($subtotal); ?></p>
						</li>
						<li class="payment_details_item">
							<p>Discount:</p>
							<p>- <?php echo wc_price($discountTotal); ?></p>
						</li>
						<?php
						if (is_user_logged_in()) {
							$discountFirstOrder = get_post_meta($order_id, 'discount_order_first', true);
							if(!empty($discountFirstOrder)) { 
								?>
								<li class="payment_details_item">
									<p>Discount for first order -30%:</p>
									<p>- <?php echo wc_price($discountFirstOrder); ?></p>
								</li>
								<?php
							}
						}
						?>
						<?php
						$tax_items = $order->get_tax_totals();
						foreach ($tax_items as $tax_item) {
							$tax_label = $tax_item->label;
							$tax_amount = $tax_item->amount;
							echo '<li class="payment_details_item">';
							echo '<p>'.$tax_label.'</p>';
							echo '<p>'.wc_price($tax_amount).'</p>';
							echo '</li>';
						}
						?>
						<li class="payment_details_item">
							<p>Delivery</p>
							<p><?php echo wc_price($order->get_shipping_total()); ?></p>
						</li>
					</ul>
					<hr>
					<div class="payment_details_total">
						<h4>Order Total</h4>
						<h4><?php echo wc_price($order->get_total()); ?></h4>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php else : ?>
		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
	<?php endif; ?>
	<div class="continue_shopping_btn">
		<a class="elementor-button elementor-button-link elementor-size-sm" href="<?php echo home_url() ?>/shop">
			<span class="elementor-button-content-wrapper">
				<span class="elementor-button-text">Continue Shopping</span>
			</span>
		</a>
	</div>
</div>
<style type="text/css">
#content{
	max-width: 90% !important;
}
#content .page-header{
	display: none;
}
#checkout-title{
	display: none;
}
.elementor-element-14298ac{
	justify-content: space-between;
	align-items: center;
	align-content: center;
	flex-direction: row;
	padding: 0;
	margin-bottom: 20px;
	margin-top: 50px;
}
.elementor-element-14298ac .elementor-element-92ca229 .elementor-heading-title{
	padding-bottom: 10px;
	width: fit-content;
	color: var(--e-global-color-text);
	border-bottom: 4px solid #FF9500;
}
.elementor-element-14298ac .elementor-element-92ca229, .elementor-element-14298ac .elementor-element-b316c86{
	width: 50%;
}
.elementor-element-14298ac .elementor-element-b316c86{
	text-align: right;
}
.order_thankyou{
	margin-bottom: 50px;
}
.order_thankyou h4{
	font-weight: bold;
}
.order_thankyou .order_summary_wrap{
	width: 100%;
}
.order_thankyou ul, li{
	list-style: none;
	padding: 0;
}
.order_thankyou table .image_product_title{
	font-weight: bold;
}
.order_thankyou .payment_details_item, .order_thankyou .summary_order_item{
	display: flex;
	flex-wrap: nowrap;
	justify-content: space-between;
	align-items: center;
	width: 100%;
}
.order_thankyou .order_details table{
	border-collapse: collapse !important;
	width: 100%;
}
.order_thankyou .order_details table, th, td {
	border: none !important;
}
.order_thankyou .order_details th, td {
	padding: 8px;
	text-align: left;

}
.order_thankyou  .order_note{
	width: 100%;
	margin-top: 20px;
}
.order_thankyou .order_details th h4{
	margin: 0 !important;
}
.order_thankyou .order_details table img.img-product-item{
	width: 148px;
	height: 100px;
	object-fit: contain;
}
.order_thankyou table tbody > tr:nth-child(odd) > td, 
.order_thankyou table tbody > tr:nth-child(odd) > td{
	background-color: #fff !important;
}
.order_thankyou table tbody tr{
	padding: 10px 0px;
	border-top: 1px solid #eaeaea;
	border-bottom: 1px solid #eaeaea;
	font-size: 16px;
}
.order_thankyou .payment_details, .order_thankyou .order_details{
	margin-top: 50px;
}
.order_thankyou .payment_details_total{
	display: flex;
	flex-wrap: nowrap;
	justify-content: space-between;
}
.continue_shopping_btn{
	text-align: center;
	margin-bottom: 50px;
}
.continue_shopping_btn a{
	text-decoration: none;
}
.moblie_view.view_small{
		display:none !important;
	}
@media(max-width: 768px){
	.desktop-view.view-large{
		display:none !important;
	}
	.moblie_view.view_small{
		display:block !important;
	}
	.moblie_view.view_small .wcspc-item{
		padding-bottom: 20px;
		border-bottom: 1px solid #808080;
	}
	.moblie_view.view_small .wcspc-item .wcspc-item-inner{
		display: flex;
		flex-wrap: wrap;
	}
	.moblie_view.view_small .wcspc-item .wcspc-item-thumb{
		width: 40%;
		padding-right: 20px;
	}
	.moblie_view.view_small .wcspc-item .wcspc-item-info{
		width: 60%;
		padding-left: 10px;
		line-height: 20px;
	}
	.moblie_view.view_small .wcspc-item .wcspc-item-info p{
		line-height: 25px;
	}
	.moblie_view.view_small .wcspc-item .wcspc-item-qty{
		width: 100%;
		text-align: right;
		margin-top: 10px;
		font-weight: bold;
	}
	.moblie_view.view_small .wcspc-item .wcspc-item-info .wcspc-item-title{
		font-weight: bold;
		color: #000;
	}
	.elementor-element-14298ac{
		margin-top: 10px;
	}
}
</style>
