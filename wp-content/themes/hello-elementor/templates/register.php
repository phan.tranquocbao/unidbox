<?php
/**
 * Template Name: Register
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
get_header();
if(is_user_logged_in()){ 
    wp_redirect(home_url()."/my-account");
}else{
    if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>
<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
<div class="register-page elementor-element e-con-full e-flex e-con e-parent" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;container_type&quot;:&quot;flex&quot;}" data-core-v316-plus="true">
    <div class="banner_register_form">
        <img src="/user12/unidbox/wp-content/uploads/2024/01/Frame-1541.png" alt="banner-desktop" class="banner-desktop">
        <img src="/user12/unidbox/wp-content/uploads/2024/02/Screenshot-2024-02-01-105318-e1706759744161.png" alt="banner-mobile" class="banner-mobile">
    </div>
    <div class="div-wrap-form-register form-register-area">
        <div class="col-2 col-form-register">
            <h2 class="title_register elementor-size-default" style=""><?php esc_html_e( 'Sign Up Now', 'woocommerce' ); ?></h2>
            <form method="post"  enctype="multipart/form-data" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >
                <?php do_action( 'woocommerce_register_form_start' ); ?>
                <div class="woocommerce-form-row row-2-col woocommerce-form-row--wide form-row form-row-wide">
                    <div class="input-wrapper">
                        <label for="first_name"><?php _e('First Name', 'text-domain'); ?><span class="required">*</span></label>
                        <input type="text" class="input-text" name="billing_first_name" id="billing_first_name" value="<?php if (isset($_POST['billing_first_name'])) echo esc_attr($_POST['billing_first_name']); ?>" placeholder="First Name" required/>
                    </div>
                    <div class="input-wrapper">
                        <label for="last_name"><?php _e('Last Name', 'text-domain'); ?><span class="required">*</span></label>
                        <input type="text" class="input-text" name="billing_last_name" id="billing_last_name" value="<?php if (isset($_POST['billing_last_name'])) echo esc_attr($_POST['billing_last_name']); ?>" placeholder="Last Name" required/>
                    </div>
                </div>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="billing_address_1"><?php esc_html_e( 'Street Address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_address_1" id="billing_address_1" value="<?php if (isset($_POST['billing_address_1'])) echo esc_attr($_POST['billing_address_1']); ?>" placeholder="Please enter a valid street address with unit no." required/><?php // @codingStandardsIgnoreLine ?>
                </p>

                <div class="woocommerce-form-row row-2-col woocommerce-form-row--wide form-row form-row-wide">
                    <div class="input-wrapper">
                        <label for="billing_postcode"><?php _e('Postal Code', 'text-domain'); ?><span class="required">*</span></label>
                        <input type="text" class="input-text" name="billing_postcode" id="billing_postcode" placeholder="Please enter a valid postal code" value="<?php if (isset($_POST['billing_postcode'])) echo esc_attr($_POST['billing_postcode']); ?>" required/>
                    </div>
                    <div class="input-wrapper">
                        <label for=""><?php _e('Contact Number', 'text-domain'); ?><span class="required">*</span></label>
                        <input type="text" class="input-text" name="billing_phone" id="billing_phone" placeholder="Please enter a valid mobile number" value="<?php if (isset($_POST['billing_phone'])) echo esc_attr($_POST['billing_phone']); ?>" required/>
                    </div>
                </div>

                <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                    </p>
                <?php endif; ?>
    
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_email"><?php esc_html_e( 'Email Address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" placeholder="username@gmail.com" required/><?php // @codingStandardsIgnoreLine ?>
                </p>
    
                <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
    
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="reg_password"><?php esc_html_e( 'Setup Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" placeholder="Please enter 8 or more characters with special characters" required/>
                    </p>
    
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="password_2"><?php _e( 'Re-type Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                        <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="confirm_password" id="confirm_password" autocomplete="new-password" placeholder="Re-type your Password"/>
                    </p>
                <?php else : ?>
    
                    <p style="font-size: 14px; margin-left: 4px;"><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>
    
                <?php endif; ?>
                <script src="https://www.google.com/recaptcha/api.js?render=6LeO1lAnAAAAAKQk8sj6bJVc8oCVh1OsSnHWvIj4"></script>
                <script>
                    grecaptcha.ready(function () {
                        grecaptcha.execute('6LeO1lAnAAAAAKQk8sj6bJVc8oCVh1OsSnHWvIj4', {action: 'register'}).then(function (token) {
                            document.getElementById('g-recaptcha-response').value = token;
                        });
                    });
                </script>
                <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
                <?php do_action( 'woocommerce_register_form' ); ?>
    
                <p class="woocommerce-form-row form-row submit-btn-reg">
                    <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                    <button type="submit" class="elementor-button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Sign Up', 'woocommerce' ); ?>"><?php esc_html_e( 'Sign Up', 'woocommerce' ); ?></button>
        <!--             <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="woocommerce-button button woocommerce-form-login" name="login"><?php esc_attr_e( 'Log in', 'woocommerce' ); ?></a> -->
                </p>
                <?php do_action( 'woocommerce_register_form_end' ); ?>
                <?php echo do_shortcode("[login-social]"); ?>

                <p class="have-an-account">Have an account ?<a href="<?php echo home_url();?>/login"><br>Sign In</a></p>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function () {
    var textOnlyInput = document.getElementById('billing_first_name');
    var textOnlyInputl = document.getElementById('billing_last_name');
    var textOnlyNumber = document.getElementById('billing_phone');
    textOnlyNumber.addEventListener('input', function () {
        var inputValue = this.value;
        var numericValue = inputValue.replace(/\D/g, '');
        this.value = numericValue;
    });
    textOnlyInput.addEventListener('input', function () {
        var inputValue = this.value;
        var textValue = inputValue.replace(/[0-9]/g, '');
        this.value = textValue;
    });
    textOnlyInputl.addEventListener('input', function () {
        var inputValue = this.value;
        var textValue = inputValue.replace(/[0-9]/g, '');
        this.value = textValue;
    });
});
</script>
<style>
.register-page.e-con-full{
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
}
.banner_register_form{
    width: 90%;
    min-height: 30vh;
    padding: 0 30px;
    /* background-image: url("/user12/unidbox/wp-content/uploads/2024/01/Frame-1541.png");
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    border-radius: 25px; */
    margin: auto;
}
.col-form-register{
    width: 100%;
    max-width: 600px;
    padding: 30px;
}
p.have-an-account{
    text-align: center;
    color: var(--e-global-color-d85c849, #253D4E);
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: 24px;
}
.banner-mobile {
    display: none;
}
.banner-desktop {
    display: block;
}
.col-form-register h2.title_register{
    text-align: center;
    font-size: 36px; font-weight:700;
    color: var(--e-global-color-d85c849);
}
.woocommerce-notices-wrapper {
    padding: 0 40px;
}
.wc-block-components-notice-banner.is-success {
    width: 90%;
}
@media (max-width: 768px) {
    .woocommerce-notices-wrapper {
        padding: 0 20px;
    }
    .wc-block-components-notice-banner.is-success {
        width: 100%;
    }
}
@media (max-width: 880px) {
    .col-form-register h2.title_register{
        font-size: 28px; 
    }
    .banner-mobile {
        display: block;
    }
    .banner-desktop {
        display: none;
    }
    p.have-an-account{
        font-size: 12px;
        line-height: 1;
    }

    .banner_register_form{
        width: 100%;
        min-height: 30vh;
        padding: 0 10px;
    }
    .col-form-register{
        padding: 10px;
    }
}

.banner_register_form > img {
    width: 100%;
    height: auto;
}
.form-register-area{
    display: flex;
    justify-content: center;
}


.row-2-col{
    display: flex;
    flex-wrap: nowrap;
    justify-content: space-between;
    margin-block-end:0.9rem
}
.row-2-col .input-wrapper{
    width: 49%;
}
.woocommerce-privacy-policy-text{
    display: none;
}
.woocommerce-form-register__submit{
    float: none !important;
    margin-top: 0.9rem;
    width: 100%;
    font-size: 16px;
}

.woocommerce-form-register input{
    border: 1px solid #C4C4C4 !important;
}
</style>
<?php endif;
}
get_footer();