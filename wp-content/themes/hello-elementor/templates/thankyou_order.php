<?php

/**
 * Template Name: Thankyou Order
 *
 */
if (!defined("ABSPATH")) {
    exit; // Exit if accessed directly.
}
get_header();
$order_id= $_GET['order_id'];
$key= $_GET['key'];
$popup_id= $_GET['popup'];
if(isset($popup_id)){
	if($popup_id != md5('checkout!Unidbox@@success')){
        wp_redirect( home_url());
		exit;
    }
	$redirect_url = home_url( '/checkout/order-received/' . $order_id . '/?key=' . $_GET['key'] . '&popup=' . $popup_id );
}

?>
<div class="elementor-element elementor-element-459b6b5 e-flex e-con" data-element_type="container"  data-core-v316-plus="true">
	<div class="elementor-element elementor-element-cd0cc2b e-flex e-con"  data-element_type="container" >
		<div class="elementor-element elementor-widget__width-inherit elementor-view-default elementor-widget elementor-widget-icon" >
			<div class="elementor-widget-container">
				<div class="elementor-icon-wrapper">
		            <div class="elementor-icon">
			            <i aria-hidden="true" class="fas fa-check-circle"></i>			
                    </div>
		        </div>
			</div>
		</div>
		<div class="elementor-element elementor-widget__width-inherit elementor-widget elementor-widget-heading"  data-element_type="widget" data-widget_type="heading.default">
			<div class="elementor-widget-container">
			    <h2 class="elementor-size-default">Your Order Confirmed!</h2>		
            </div>
		</div>
		<div class="elementor-element elementor-widget__width-inherit elementor-widget elementor-widget-heading" data-element_type="widget" data-widget_type="heading.default">
			<div class="elementor-widget-container">
			    <p class="elementor-size-default">We sent a confirmation email to your inbox. Do look out for it.</p>		
            </div>
		</div>
		<div class="elementor-element elementor-widget elementor-widget-button" data-element_type="widget" data-widget_type="button.default">
			<div class="elementor-widget-container">
				<div class="elementor-button-wrapper">
			        <a class="elementor-button elementor-button-link elementor-size-sm" href="<?php echo $redirect_url; ?>">
                        <span class="elementor-button-content-wrapper">
                            <span class="elementor-button-text">Go to Order Confirmation Page</span>
                        </span>
					</a>
		        </div>
			</div>
		</div>
	</div>
</div>
<style>
.elementor-element-459b6b5{
	display:flex;
	align-content:center;
    justify-content: center;
    padding: 0 5%;
    padding: 50px 0px;
	background-image: url("/user12/unidbox/wp-content/uploads/2024/01/Pexels-Photo-by-Vecislavas-Popa-1.png");
	background-position:center;
	background-repeat:no-repeat;
	background-size:cover;
}


.elementor-element-cd0cc2b{
    max-width: 500px;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.3);
    border-radius: 8px;
    background-color: #fff;
    padding: 50px;
    justify-content: center;
    flex-wrap: wrap;
    height: fit-content;
}
.elementor-element-cd0cc2b .elementor-widget__width-inherit{
    margin-bottom: 20px !important;
	text-align:center;
}
.elementor-element-cd0cc2b .elementor-icon i{
    color: rgba(23, 192, 159, 1) !important;
}
.elementor-element-cd0cc2b h2, .elementor-element-cd0cc2b p{
    color: #000 !important;
    text-align: center;
}
.elementor-element-cd0cc2b p{
    font-size: 16px;
    font-weight: 400;
}
.elementor-element-cd0cc2b a.elementor-button-link{
    border: 1px solid rgba(128, 128, 128, 1);
    background-color: #fff !important;
    color: #000;
    box-shadow: unset;
}
.elementor-element-cd0cc2b .elementor-widget-button .elementor-button-wrapper{
	text-align:center;
}
</style>
<?php 
get_footer();
