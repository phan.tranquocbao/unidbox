<?php

/**
 * Template Name: Login
 *
 */
if (!defined("ABSPATH")) {
    exit; // Exit if accessed directly.
}
get_header();
if (is_user_logged_in()) {
    wp_redirect(home_url() . "/my-account");
} else { ?>
    <div class="div-wrap-form-login form-login-area">
		<?php do_action("woocommerce_before_customer_login_form"); ?>
        <?php echo do_shortcode("[print-login-form]"); ?>

    </div>
    <script type="text/javascript">
        // Code here...
    </script>
    <?php do_action("woocommerce_after_customer_login_form"); ?>
    <style>
        .form-login-area {
            display: flex;
			flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            min-height: 84vh;
            background-image: url("/user12/unidbox/wp-content/uploads/2024/01/Pexels-Photo-by-Vecislavas-Popa-1.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        .form-login-area  > .woocommerce-notices-wrapper {
            width: 90%;
            margin: auto;
            padding: 0 40px;
        }
        @media (max-width: 880px) {
            .form-login-area > .woocommerce-notices-wrapper {
                width: 100%;
                padding: 0;
            }
        }
        
    </style>
<?php }
get_footer();
