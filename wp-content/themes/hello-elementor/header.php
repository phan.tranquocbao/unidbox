<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section, opens the <body> tag and adds the site's header.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$viewport_content = apply_filters( 'hello_elementor_viewport_content', 'width=device-width, initial-scale=1' );
$enable_skip_link = apply_filters( 'hello_elementor_enable_skip_link', true );
$skip_link_url = apply_filters( 'hello_elementor_skip_link_url', '#content' );
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="<?php echo esc_attr( $viewport_content ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha384-rDp5KoyiApgoCkFaa9xPBmHCOKAveE9l+K4/T5xkO1BR8DlQYq2UqUV3lZzKrZzR" crossorigin="anonymous">
<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<?php wp_head(); ?>
</head>
	<style>
    .mt-150{
        margin-top: 50px;
    }
    .show{
        display: block !important;
    }
</style>
<script>
    // Function to toggle subcategories
    function toggleSubcategories(element, id) {

        var subcategoriesContainer = $("#subcategories-" + id);
        subcategoriesContainer.addClass('mt-150');
        var subcategoriesNext = $(element).next();
        subcategoriesNext.slideToggle();

        var dataParentValue = $(element).data("parent");
        $("#back-cat-"+dataParentValue).hide();
        $("#subcategories-" + dataParentValue).removeClass('mt-150');
        $("#subcategories-" + dataParentValue).css("margin-left", "0px");

        subcategoriesContainer.find('.category-list-custom .category-list-custom-item a.sub_item-ind').attr("data-parent", id);
        subcategoriesContainer.find('.category-list-custom .category-list-custom-item a.back-button').attr("data-parent", id);

		$("#menu-service-main").hide();

        var category_parent = $('#category_parent-'+id);

        // $("#subcategories-"+id).attr("data-parent", id);
        category_parent.addClass('show');
        $('.category-list-custom-item').not(category_parent.find('.category-list-custom-item')).hide();
        category_parent.show();
        category_parent.children('a').hide();

        // $('.category-list-custom-item').hide();
        // $('#category_parent-'+id +' .category-list-custom-item').show();

    }

    // Function to go back
    function goBack(button, id) {
        var subcategoriesContainer = $(button).parent();
        subcategoriesContainer.slideToggle();
        var dataParentValue = $(button).data("parent");

        $("#back-cat-"+dataParentValue).show();
        $("#subcategories-" + dataParentValue).addClass('mt-150');

        $("#subcategories-" + dataParentValue).css("margin-left", "20px");

        if ($(button).hasClass('child-cat')) {
            $("#menu-service-main").slideToggle();
            $('.category-list-custom-item').show();
            $('#category_parent-'+id).removeClass('show');
        }
        $('#category_parent-'+id).removeClass('show');
        $('#category_parent-'+id).show();
        var targetLiElement = $('#category_parent-'+id);
        targetLiElement.children('a').show();
        targetLiElement.siblings().show();
    }
</script>
<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php if ( $enable_skip_link ) { ?>
<a class="skip-link screen-reader-text" href="<?php echo esc_url( $skip_link_url ); ?>"><?php echo esc_html__( 'Skip to content', 'hello-elementor' ); ?></a>
<?php } ?>

<?php
if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'header' ) ) {
	if ( hello_elementor_display_header_footer() ) {
		if ( did_action( 'elementor/loaded' ) && hello_header_footer_experiment_active() ) {
			get_template_part( 'template-parts/dynamic-header' );
		} else {
			get_template_part( 'template-parts/header' );
		}
	}
}
