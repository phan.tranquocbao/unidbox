<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.5.0/dist/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.5.0/dist/css/bootstrap.min.css">
<!-- Add Popper.js (for Bootstrap's dropdowns, popovers, and tooltips) -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@2.11.6/dist/umd/popper.min.js"></script>

<h2 style="margin-bottom: 30px;">Coupon Emails (Discount 30%)</h2>
<?php
global $wpdb;

// item per page 
$items_per_page = 30;

// current page 
$current_page = isset($_GET['paged']) ? $_GET['paged'] : 1;

// offset
$offset = ($current_page - 1) * $items_per_page;

$table = $wpdb->prefix . 'email_coupon_saleoff';    
$sql = "SELECT * FROM $table ";
$total_items = $wpdb->get_var("SELECT COUNT(*) FROM $table");
$message_filter = '';
$where_conditions = array();
// filter and get data
if (isset($_POST['search_submit'])) {
    $search_term = sanitize_text_field($_POST['search_input']);
    $where_conditions[] = "email_address LIKE '%$search_term%'";
    $message_filter = 'Filter with email "'.$search_term.'"';
}

// Date filter
if (isset($_POST['filter_date_submit'])) {
    $form_date = isset($_POST['form_date']) ? date('Y-m-d', strtotime($_POST['form_date'])) : '';
    $to_date = isset($_POST['to_date']) ? date('Y-m-d', strtotime($_POST['to_date'])) : '';
    $where_conditions[] = "date_send BETWEEN '$form_date' AND '$to_date'";
    $message_filter = 'Filter data from date '.formatDate($form_date).' to '.formatDate($to_date);
}

// Status filter
if (isset($_POST['filter_status_submit'])) {
    $status_filter = isset($_POST['status_filter']) ? sanitize_text_field($_POST['status_filter']) : '';
    if ($status_filter === 'used') {
        $where_conditions[] = "id_order IS NOT NULL";
        $message_filter = 'Filter with status Used';
    } elseif ($status_filter === 'not_used') {
        $where_conditions[] = "id_order IS NULL";
        $message_filter = 'Filter with status Not Use';
    }
}

// Combine WHERE conditions
if (!empty($where_conditions)) {
    $sql .= " WHERE " . implode(' AND ', $where_conditions);
}


$sql .= " ORDER BY date_send ASC LIMIT $items_per_page OFFSET $offset";


// Remove Emails Coupon
if (isset($_POST['selected_email_ids']) && is_array($_POST['selected_email_ids'])) {
    $selected_email_ids = $_POST['selected_email_ids']; 
    $ids_string = implode(',', $selected_email_ids);
    $wpdb->query("DELETE FROM $table WHERE id IN ($ids_string)");
}

$results_list = $wpdb->get_results($sql);

$pagination = paginate_links(array(
    'base' => add_query_arg('paged', '%#%'),
    'format' => '',
    'prev_text' => __('&laquo; Previous'),
    'next_text' => __('Next &raquo;'),
    'total' => ceil($total_items / $items_per_page),
    'current' => $current_page,
));

function formatDateTime($string){
    $date = new DateTime($string);
    $formatted_date = $date->format('M j Y - h:i A');
    return $formatted_date;
}
function formatDate($string){
    $date = new DateTime($string);
    $formatted_date = $date->format('M j Y');
    return $formatted_date;
}

?>
<div style="margin-bottom: 15px;">
    <div class="search-coupon">
        <form method="post" action="" enctype="multipart/form-data">
            <input type="text" name="search_input" placeholder="search email">
            <button type="submit" class="btn btn-primary mr-2 save-request-status" name="search_submit" id="search_submit" >Seach</button>
        </form>
        <form method="post" action="" enctype="multipart/form-data">
            <input type="date" name="form_date"> - <input type="date" name="to_date">
            <button type="submit" class="btn btn-primary mr-2 save-request-status" name="filter_date_submit" id="filter_date_submit" >Filter Date Register</button>
        </form>
        <form method="post" action="" enctype="multipart/form-data">
            <select name="status_filter">
                <option value="" selected>Select Status</option>
                <option value="used">Used</option>
                <option value="not_used">Not Use</option>
            </select>
            <button type="submit" class="btn btn-primary mr-2 save-request-status" name="filter_status_submit" id="filter_status_submit">Filter Status</button>
        </form>
    </div>
</div>
<div class="message-filter">
    <h4 id="message-filter-list"><?php echo $message_filter; ?></h4>
    <button type="submit" value="Delete" style="margin-bottom: 10px;" class="btn btn-danger save-request-status" id="removeAllCoupon" data-dismiss="modal">Delete Emails Selected</button>
</div>
<div id="table_meeting">
        <div class="scroll-x">
            <table  id="table_schedule_meeting" class="table table-striped table-bordered" border="1" style="width:100%; border-collapse:collapse;" cellpadding="10">   
                <thead style="background-color:#000; color: #fff;">
                    <tr>
                        <th><input type="checkbox" id="list-check-all" style="border-radius: 0; width: 15px; height: 15px; -webkit-appearance: listbox;"></th>
                        <th>No.</th>   
                        <th>Email Address</th>  
                        <th>Date Register</th>
                        <th>Coupon Code</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($results_list): ?>
                        <?php 
                            $n = 1;
                            foreach ($results_list as $key => $row): ?>
                            <tr>
                                <td><input type="checkbox" value="<?= $row->id ?>" class="coupon-checkbox"></td>
                                <td><?php echo $n++ ?></td>
                                <td><?php echo $row->email_address ?></td>
                                <td><?php echo $row->date_send ? formatDateTime($row->date_send) : 'Unknown'; ?></td>
                                <td><strong><?php echo $row->coupon_code ? '<a href="' . home_url('wp-admin/post.php?post=' . $row->id_coupon . '&action=edit') . '">' . $row->coupon_code . '</a>' : 'Unknow'; ?> </strong></td>
                                <td><?php echo $row->id_order ? 'Used In order <strong><a href="' . home_url('wp-admin/post.php?post=' . $row->id_order . '&action=edit') . '">#'. $row->id_order . '</a></strong>' : 'Not use'; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr><td colspan="6">No data to show.</td></tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    <?php
    ?>
</div>
<!-- Modal Remove All Emails -->
<div id="removeMultipleEmailModal" class="modal">
    <div class="modal-content">
        <form method="post" id="remove-multiple-email-form" action="" enctype="multipart/form-data">
            <div class="modal-body">
                <h4>Are you sure you will delete these booking?</h4>
                <input type="hidden" name="selected_email_ids[]" value="">
            </div>
            <div class="modal-footer">
                <div class="in-line">
                    <div  class="m-r-a">
                        <button type="button" class="btn mr-2 close-modal" data-dismiss="modal" onclick="closeModal('#removeMultipleEmailModal')">Close</button>
                    </div>
                    <div> 
                        <button type="submit" value="Delete" class="btn btn-danger save-request-status" id="submit_delete" data-dismiss="modal">Delete</button>
                    </div>
                </div>
            </div>
        </form>
	</div>
</div>
<?php
    echo 
    '<div style="display: flex; justify-content: end;">
        <div class="pagination" id="pagination" style="margin-right: 10px;">' . $pagination . '</div>
    </div>';

?>
<script type="text/javascript">
function closeModal(id){
    jQuery(id).css('display','none');
}

jQuery(document).ready(function() { 
    jQuery("#removeAllCoupon").click(function() {
        if (jQuery(".coupon-checkbox:checked").length > 0) {
            jQuery('#removeMultipleEmailModal').css('display', 'block');
            let selectedEmailIds = [];

            jQuery(".coupon-checkbox:checked").each(function() {
                selectedEmailIds.push(jQuery(this).val()); 
            });

            jQuery("#remove-multiple-email-form input[name='selected_email_ids[]']").val(selectedEmailIds.join(','));
        }
    });
});
jQuery("#list-check-all").change(function() {

    let isChecked = jQuery(this).prop("checked");

    jQuery(".coupon-checkbox").prop("checked", isChecked);
});
</script>
<style>
.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	padding-top: 150px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0,0,0); /* Fallback color */
	background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
	position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    pointer-events: auto;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 0.3rem;
	outline: 0;
	margin: auto;
    padding: 20px;
    max-width: 600px;
}

.modal-content h3{
	text-align:center
}
.modal-header{
	-ms-flex-align: start;
	align-items: flex-start;
	-ms-flex-pack: justify;
	justify-content: space-between;
	border-bottom: 1px solid #e9ecef;
	display: block;
}
.modal-footer {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-align: center;
	-ms-flex-align: center;
	align-items: center;
	-webkit-box-pack: end;
	-ms-flex-pack: end;
	justify-content: flex-end;
	padding: 1rem 1rem 1rem 0;
	border-top: 1px solid #e9ecef;
}
.in-line{
    display: flex;
    width: 100%;
}
.m-l-a{
    margin-left: auto;
}
#wpbody-content{
    padding-right: 3%;
}
.message-filter{
    display: flex;
    flex-wrap: nowrap;
    align-items: center;
    justify-content: space-between;
}
.search-coupon{
    display: flex;
    flex-wrap: nowrap;
    align-items: center;
    justify-content: flex-start;
}
.search-coupon form{
    margin-right: 30px;
}
.search-coupon input, .search-coupon select{
    min-height: 33px !important;
}
.close-modal{
    margin-right: 10px;
}
.coupon-checkbox{
    border-radius: 0 !important;
    width: 15px !important;
    height: 15px !important;
    -webkit-appearance: listbox !important;
}
</style>