<?php

function discount_percent($percent, $price) {
    return $price * ($percent / 100);
}

add_action('woocommerce_created_customer', function($customer_id){
    global $wpdb;
    $email = get_userdata($customer_id)->user_email;
    
    $table = $wpdb->prefix . 'email_coupon_saleoff';
    $sql = $wpdb->prepare("SELECT * FROM $table WHERE email_address = %s", $email);
    $results = $wpdb->get_results($sql, OBJECT);
    
    if(empty($results)){
        // save data
        $data = array(
            'email_address' => $email, 
            'coupon_code' => null, 
            'date_send' => current_datetime()->format('Y-m-d H:i:s'),
            'date_used' => null,
            'order_id_used' => null,
            'user_id' => $customer_id
        );
        $format = array('%s', '%s', '%d', '%s', '%s', '%d');
        $wpdb->insert($table, $data, $format);
    }else{
        $wpdb->update(
            $table,
            array('user_id' => $customer_id),
            array('email_address' => $email),
            array('%d'), 
            array('%s')  
        );
    }

    // update_user_meta($customer_id, '_first_buy_order', 0);
});

add_action('woocommerce_cart_calculate_fees', function($cart){
    global $wpdb;
    if (is_admin() && !defined('DOING_AJAX')) {
        return;
    }

    if (is_user_logged_in()) {
        $user_id = get_current_user_id();
        // $first_buy_order = get_user_meta($user_id, '_first_buy_order', true);
    
        $table = $wpdb->prefix . 'email_coupon_saleoff';
        $sql = $wpdb->prepare("SELECT * FROM $table WHERE user_id = %d", $user_id);
        $results = $wpdb->get_results($sql, OBJECT);
        if($results){
            $result = $results[0];
            $first_buy_order = $result->order_id_used;
            $orders = wc_get_orders(array(
                'customer' => $user_id,
                'status'   => array('completed', 'processing', 'on-hold'),
                'payment_complete' => true
            ));
    
            $order_count = count($orders);
    
            if ($first_buy_order == null && $order_count == 0) {
                $discount = discount_percent(30, $cart->cart_contents_total);
                $cart->add_fee(__('Discount for first order -30%', 'woocommerce'), -$discount);
                WC()->session->set('discount_order_first', $discount);
            }
        }
    }
}, 10, 1);


add_action('woocommerce_checkout_update_order_meta', function($order_id){
	$discount = WC()->session->get('discount_order_first');
    if (!empty($discount)) {
        update_post_meta($order_id, 'discount_order_first', $discount);
        WC()->session->__unset('discount_order_first');
    }
}, 10, 1);


add_action('woocommerce_order_status_processing', 'update_first_order', 20, 1);
add_action('woocommerce_order_status_completed', 'update_first_order', 20, 1);
add_action('woocommerce_order_status_on-hold', 'update_first_order', 20, 1);
function update_first_order($order_id){
    global $wpdb;
	$order = wc_get_order($order_id);
	if ($order->get_user_id()) {
		$user_id = $order->get_user_id();
		$table = $wpdb->prefix . 'email_coupon_saleoff';
        $sql = $wpdb->prepare("SELECT * FROM $table WHERE user_id = %d", $user_id);
        $results = $wpdb->get_results($sql, OBJECT);
        if($results){
            $result = $results[0];
            $first_buy_order = $result->used_coupon;
            if ($first_buy_order == 0) {
				$wpdb->update(
                    $table,
                    array('date_used' => current_datetime()->format('Y-m-d H:i:s')),
                    array('user_id' => $user_id),
                    array('%s'), 
                    array('%d')  
                );
			}
        }
	}
}