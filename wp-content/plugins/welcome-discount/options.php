<?php
/*
Plugin Name: TGB Welcome Discount
Description: Bona Technologies plugin.
Author: TGB
Version: 1.0.0
*/
// require_once(plugin_dir_path(__FILE__) . 'discount.php');
// Đoạn mã xử lý Ajax trong plugin

add_shortcode('show_welcome_pop_up', function () {
    $popup_id = '9097';
    ElementorPro\Modules\Popup\Module::add_popup_to_location($popup_id);
?>
    <script>
        ;
        ((w, d) => {
            w.addEventListener('elementor/frontend/init', () => {
                const $ = jQuery;

                let popupsClosed = JSON.parse(sessionStorage.getItem('wpg_popups_closed')) || [];
                // console.log(popupsClosed);
                $(d).on('elementor/popup/hide', (e, id) => {
                    //Id id not already in session storage, add it.
                    if (false === popupsClosed.includes(id)) {
                        popupsClosed.push(id);
                        sessionStorage.setItem('wpg_popups_closed', JSON.stringify(popupsClosed));
                    }
                })
                elementorFrontend.on('components:init', () => {
                    const popupId = 9097; //ID of Popup to open
                    if (false === popupsClosed.includes(popupId)) {
                        if (elementorFrontend.documentsManager.documents[popupId]) {
                            $(document).ready(function() {
                                setTimeout(function() {
                                    $('#elementor-popup-modal-9097').css('opacity', 1);
                                    $('#elementor-popup-modal-9097').css('z-index', 10);
                                    $('#elementor-popup-modal-9097').css('display', 'flex');
                                }, 300);
                            });
                        }
                    }
                });
            })
        })(window, document)
    </script>
<?php
});

function generate_coupon($coupon_generated) {
    
    // Set some coupon data by default
    $date_expires     = '';
    $usage_limit      = 1;
    $discount_type    = 'percent'; // 'store_credit' doesn't exist
    $coupon_amount    = '30';

    $coupon = new WC_Coupon();
    
    $coupon->set_code($coupon_generated);
    $coupon->set_discount_type($discount_type);
    $coupon->set_amount($coupon_amount);
    $coupon->set_date_expires($date_expires);
    $coupon->set_usage_limit($usage_limit);

    $coupon->save();

    return $coupon_generated;
}

function tgb_welcome_discount_handling(){
    global $wpdb;
    $formData = $_POST['formData'];
    parse_str($formData, $formDataArray);
    $emailValue = urldecode($formDataArray['welcome-email']);

    $table = $wpdb->prefix . 'email_coupon_saleoff';
    $sql = $wpdb->prepare("SELECT * FROM $table WHERE email_address = %s", $emailValue);

    $results = $wpdb->get_results($sql, OBJECT);

    $response = ['message' => 'Email existed. Please try another email address', 'success' => false];
    if (empty($results)) {
        $coupon_code = rand();
        $coupon = generate_coupon($coupon_code);

        // Get the coupon ID
        $coupon_id = wc_get_coupon_id_by_code($coupon);
        // save data
        $data = array(
            'email_address' => $emailValue, 
            'coupon_code' => $coupon_code, 
            'date_send' => current_datetime()->format('Y-m-d H:i:s'),
            'id_coupon' => $coupon_id,
            'id_order' => null
        );
        $format = array('%s', '%s', '%s', '%s', '%s');
        $result_check = $wpdb->insert($table, $data, $format);

        $response = ['message' => 'Has error', 'success' => false];
        if ($result_check) {
            $to = $emailValue;
            $subject = 'Email Voucher 30%'; 
            $message = '<div style="padding: 15px; border: 1px solid #cccccc;">';
            $message .=  '<p> Hello '.$emailValue.'</p>
                <p> Congratulations! You have unlocked a 30% discount voucher for your first purchase. Below is the exclusive code ready for use at checkout:
                <br>
                <br>
                    <strong>'.$coupon.'</strong>
                <br>
                <br>
                    <small>(This voucher is only valid for 1 order)</small> </p>';
            $message .=  '</div>'; 
            $admin_email = get_option('admin_email');
            $headers = 'Content-Type: text/html; charset=UTF-8' . "\r\n";
            $headers .= 'From:'.$admin_email . "\r\n" .
                'Reply-To: '.$admin_email . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
    
            // Send the email
            $mailSent = wp_mail($to, $subject, $message, $headers);
            $msg =  $mailSent ? 'Email send successfully.' : 'Failed to send the email.';

            $response = ['message' => $msg, 'success' => $mailSent];
        }
    }
    wp_send_json($response);
    die();
}

add_action('wp_ajax_tgb_welcome_form_submit', 'tgb_welcome_discount_handling');
add_action('wp_ajax_nopriv_tgb_welcome_form_submit', 'tgb_welcome_discount_handling'); // Cho người dùng không đăng nhập

add_shortcode('tgb_welcome_discount', function () {
    ob_start();
?>
    <style>
        #welcome-button {
            background-color: var(--e-global-color-b7535d1);
            color: #ffffff;
            padding: 10px;
            width: 100%;
            border-radius: 8px;
            font-size: 16px;
            font-weight: 700;
        }

        #welcome-email-input {
            margin-bottom: 10px;
            border: none;
            outline: none;
        }

        #welcome-form {
            margin-top: 30px;
        }

        #welcome-form-message {
            color: #fff;
            font-size: 16px;
            text-align: center;
            font-style: italic;
        }
    </style>
    <form id="welcome-form">
        <input type="email" name="welcome-email" id="welcome-email-input" placeholder="Enter your email address" required="required" aria-required="true" />
        <button type="submit" id="welcome-button">Get my 30% OFF</button>
        <div id='welcome-form-message'></div>
    </form>
    <script>
        jQuery(document).ready(function($) {
            $('#welcome-form').on('submit', function(e) {
                e.preventDefault();
                $('#welcome-button').attr('disabled', 'disabled');
                $('#welcome-button').css('background-color', '#c14242');
                $('#welcome-form-message').css('margin-top', '10px');
                $('#welcome-form-message').text('Checking...');
                var formData = $('#welcome-form').serialize();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: {
                        action: 'tgb_welcome_form_submit',
                        formData: formData
                    },
                    success: function(response) {
                        $('#welcome-button').removeAttr('disabled');
                        $('#welcome-button').css('background-color', 'var(--e-global-color-b7535d1)');
                        // console.log(response);
                        $('#welcome-form-message').css('margin-top', '10px');
                        $('#welcome-form-message').text(response.message);
                        if (response.success) {
                            sessionStorage.setItem('wpg_popups_closed', JSON.stringify([9097]));
                            setTimeout(function() {
                                $('#elementor-popup-modal-9097').css('opacity', 0);
                                $('#elementor-popup-modal-9097').css('transition', '0.3s ease');
                            }, 5000);
                        }
                    }
                });
            });
        });
    </script>
<?php
    return ob_get_clean();
});

add_action('admin_menu', 'menu_admin_coupon_email');	
function menu_admin_coupon_email() {
    // This is a submenu
    add_submenu_page(
        'woocommerce', // Parent slug (use the menu slug of the parent menu)
        'Coupon Email Lists', // Page title
        'Coupon Email List', // Menu title
        'manage_options', // Capability
        'coupon_email_list', // Menu slug
        'admin_coupon_email_list' // Callback function to display the page content
    );
}

function admin_coupon_email_list(){
    require_once( plugin_dir_path(__FILE__) . 'admin_list_coupon.php');
}

add_action('woocommerce_order_status_processing', 'update_coupon_order', 20, 1);
add_action('woocommerce_order_status_completed', 'update_coupon_order', 20, 1);
add_action('woocommerce_order_status_on-hold', 'update_coupon_order', 20, 1);
function update_coupon_order($order_id){
    global $wpdb;

    $order = wc_get_order($order_id);
    $applied_coupons = $order->get_used_coupons();
    if($applied_coupons){
        $table = $wpdb->prefix . 'email_coupon_saleoff';
        $sql = $wpdb->prepare("SELECT * FROM $table WHERE coupon_code = %s", $applied_coupons);
        $coupon_data = $wpdb->get_results($sql, OBJECT);
        if (!empty($coupon_data)) {
            foreach ($coupon_data as $coupon) {
                $wpdb->update(
                    $table,
                    array('id_order' => $order_id),
                    array('coupon_code' => $coupon->coupon_code),
                    array('%d'), 
                    array('%s')  
                );
            }
        }
    }
}