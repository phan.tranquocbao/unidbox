<?php
/**
 * Plugin Name: Disable Comment Menu Wordpress
 * Description: This plugin disables the comment menu
 * Version: 1.0
 */

/**
 * Removes some menus by page.
 */
function wpdocs_remove_menus(){
   
  remove_menu_page( 'edit-comments.php' ); //Comment
  /*remove_menu_page( 'jetpack' );                    //Jetpack* 
  remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'upload.php' );                 //Media
  remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'themes.php' );                 //Appearance
  remove_menu_page( 'plugins.php' );                //Plugins
  remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  remove_menu_page( 'options-general.php' );        //Settings
   */
}
add_action( 'admin_menu', 'wpdocs_remove_menus' );