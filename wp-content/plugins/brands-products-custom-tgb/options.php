<?php
/*
Plugin Name: Custom Brands Products Functions Woocommerce
Description: Bona Technologies plugin.
Author: TGB
Version: 1.0.0
*/

add_shortcode('job_positions_type_list', function () {
    $current_category = get_queried_object();
    $current_category_id = $current_category->term_id;

    $args = array(
        'taxonomy' => 'job-positions-category',
        'hide_empty' => false,
    );
    $categories = get_categories($args);

    $selected_job_type = isset($_GET['job_type']) ? sanitize_text_field($_GET['job_type']) : '';

    $search_query = isset($_GET['job_search']) ? sanitize_text_field($_GET['job_search']) : '';

    ob_start();

    echo '<form method="get" id="job_positions_filter_form">';
	echo '<input type="hidden" name="job_type" value="' . esc_attr($selected_job_type) . '">';
    echo '<div class="dropdown">';
    echo '<button class="btn btn-secondary dropdown-toggle" type="button" id="job_type_filter_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    echo 'Employment Type';
    echo '</button>';
    echo '<div class="dropdown-menu" aria-labelledby="job_type_filter_button" id="job_type_filter_menu">';
    echo '<ul>';
    echo '<li class="' . ($_GET['job_type'] == 'full-time' ? "active" : "") . '"><a href="' . esc_url(add_query_arg('job_type', 'full-time')) . '">Full-Time</a><i class="' . ($_GET['job_type'] == 'full-time' ? "fas fa-dot-circle" : "far fa-circle") . '"></i></li>';
    echo '<li class="' . ($_GET['job_type'] == 'part-time' ? "active" : "") . '"><a href="' . esc_url(add_query_arg('job_type', 'part-time')) . '">Part-Time</a><i class="' . ($_GET['job_type'] == 'part-time' ? "fas fa-dot-circle" : "far fa-circle") . '"></i></li>';
    echo '<li class="' . ($_GET['job_type'] == 'contract' ? "active" : "") . '"><a href="' . esc_url(add_query_arg('job_type', 'contract')) . '">Contract</a><i class="' . ($_GET['job_type'] == 'contract' ? "fas fa-dot-circle" : "far fa-circle") . '"></i></li>';
    echo '<li class="' . ($_GET['job_type'] == 'freelance' ? "active" : "") . '"><a href="' . esc_url(add_query_arg('job_type', 'freelance')) . '">Freelance</a><i class="' . ($_GET['job_type'] == 'freelance' ? "fas fa-dot-circle" : "far fa-circle") . '"></i></li>';
    echo '<li class="' . ($_GET['job_type'] == 'internship' ? "active" : "") . '"><a href="' . esc_url(add_query_arg('job_type', 'internship')) . '">Internship</a><i class="' . ($_GET['job_type'] == 'internship' ? "fas fa-dot-circle" : "far fa-circle") . '"></i></li>';

    echo '</ul>';
    echo '</div>';
    echo '</div>';
    echo '<div class="elementor-element elementor-element-b45a39d elementor-search-form--skin-classic elementor-search-form--button-type-icon elementor-search-form--icon-search elementor-widget elementor-widget-search-form" data-id="b45a39d" data-element_type="widget" id="job-search-form" data-settings="{&quot;skin&quot;:&quot;classic&quot;}" data-widget_type="search-form.default">
    <div class="elementor-widget-container">
    <div class="elementor-search-form__container">
    <input placeholder="Type here to search" class="elementor-search-form__input" type="search" name="job_search" title="Search" value="' . $search_query . '">
    <button class="elementor-search-form__submit" type="submit" title="Search" aria-label="Search">
    <i aria-hidden="true" class="fas fa-search"></i> <span class="elementor-screen-only">Search</span>
    </button>
    </div>
    </div>
    </div>';
    echo '</form>';

    $categories_with_posts = array_filter($categories, function ($category) use ($selected_job_type, $search_query) {
        $category_posts_args = array(
            'post_type' => 'job-position', 
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'job-positions-category',
                    'field' => 'term_id',
                    'terms' => $category->term_id,
                ),
            ),
//             's' => $search_query, 
// 			'search_columns' => ['post_title']
        );
		
		if (!empty($search_query)) {
           	$category_posts_args['s'] = $search_query;
			$category_posts_args['search_columns'] = array('post_title');
        }

        if (!empty($selected_job_type)) {
            $category_posts_args['meta_query'][] = array(
                'key' => 'type', 
                'value' => $selected_job_type,
                'compare' => '=',
            );
        }

        $category_posts = get_posts($category_posts_args);
        return !empty($category_posts);
    });

    if ($categories_with_posts) {
        echo '<ul class="cate_ul">';
        foreach ($categories_with_posts as $category) {
            $category_link = get_term_link($category, 'job-positions-category');
            $active_class = ($current_category_id == $category->term_id) ? 'active' : '';

            echo '<li class="' . esc_attr($active_class) . '">
                <a class="category-list-custom-item-url">' . esc_html($category->name) . '</a>';

            $category_posts_args = array(
                'post_type' => 'job-position', 
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'job-positions-category',
                        'field' => 'term_id',
                        'terms' => $category->term_id,
                    ),
                ),
//                 's' => $search_query, 
// 				'search_columns' => ['post_title']
            );
			if (!empty($search_query)) {
				$category_posts_args['s'] = $search_query;
				$category_posts_args['search_columns'] = array('post_title');
			}
            if (!empty($selected_job_type)) {
                $category_posts_args['meta_query'][] = array(
                    'key' => 'type', 
                    'value' => $selected_job_type,
                    'compare' => '=',
                );
            }

            $category_posts = get_posts($category_posts_args);

            if ($category_posts) {
                echo '<ul class="post_ul">';
                foreach ($category_posts as $post) {
                    echo '<li><div class="job-position-row"><a class="job-position-row-link" href="' . esc_url(get_permalink($post->ID)) . '"><span>' . esc_html($post->post_title) . '</span><span>' . esc_html(get_post_meta($post->ID, 'type', true)) . '</span><span><svg xmlns="http://www.w3.org/2000/svg" width="11" height="16" viewBox="0 0 11 16" fill="none">
                        <path d="M5.5 0.5C2.45929 0.5 0 2.8475 0 5.75C0 9.6875 5.5 15.5 5.5 15.5C5.5 15.5 11 9.6875 11 5.75C11 2.8475 8.54071 0.5 5.5 0.5ZM5.5 7.625C4.41571 7.625 3.53571 6.785 3.53571 5.75C3.53571 4.715 4.41571 3.875 5.5 3.875C6.58429 3.875 7.46429 4.715 7.46429 5.75C7.46429 6.785 6.58429 7.625 5.5 7.625Z" fill="#253D4E"/>
                        </svg>&nbsp;&nbsp;' . esc_html(get_post_meta($post->ID, 'address', true)) . '</span></a><a class="job-position-row-detail-link job-position-row-detail-link-desktop elementor-button" href="' . esc_url(get_permalink($post->ID)) . '">Find out more</a><a class="job-position-row-detail-link job-position-row-detail-link-mobile elementor-button" href="' . esc_url(get_permalink($post->ID)) . '"><i class="fas fa-chevron-right"></i></a></div></li>';
                }
                echo '</ul>';
            }

            echo '</li>';
        }
        echo '</ul>';
    } else {
        echo 'No categories found with posts.';
    }
?>

    <script>
        jQuery(document).ready(function($) {
            $('#job_type_filter_button').click(function() {
                $('#job_type_filter_menu').toggleClass('show');
            });
            $('#job_type_filter_menu a').click(function(e) {
                e.preventDefault();
                window.location.href = $(this).attr('href');
            });
            $('#job-search-form .elementor-search-form__submit').click(function(e) {
                e.preventDefault();
                $('#job_positions_filter_form').submit();
            });
            $('#job_type_filter').change(function() {
                $('#job_positions_filter_form').submit();
            });
        });
    </script>
<?php
    return ob_get_clean();
});

add_shortcode('shop_by_brand', function () {
    $terms = get_terms(array(
        'taxonomy' => 'brand',
        'hide_empty' => false,
    ));
    
    ob_start(); // Start output buffering
    ?>
    <div class="letter-filter">
        <div class="letter-filter__letters">
            <span class="active" data-letter="A">A</span>
            <span data-letter="B">B</span>
            <span data-letter="C">C</span>
            <span data-letter="D">D</span>
            <span data-letter="E">E</span>
            <span data-letter="F">F</span>
            <span data-letter="G">G</span>
            <span data-letter="H">H</span>
            <span data-letter="I">I</span>
            <span data-letter="J">J</span>
            <span data-letter="K">K</span>
            <span data-letter="L">L</span>
            <span data-letter="M">M</span>
            <span data-letter="N">N</span>
            <span data-letter="O">O</span>
            <span data-letter="P">P</span>
            <span data-letter="Q">Q</span>
            <span data-letter="R">R</span>
            <span data-letter="S">S</span>
            <span data-letter="T">T</span>
            <span data-letter="U">U</span>
            <span data-letter="V">V</span>
            <span data-letter="W">W</span>
            <span data-letter="X">X</span>
            <span data-letter="Y">Y</span>
            <span data-letter="Z">Z</span>
        </div>
    
    <?php
    if (!empty($terms) && !is_wp_error($terms)) {
        ?>
        <ul class="tgb-brands-container">
            <?php
            foreach ($terms as $term) {
                $term_link = get_term_link($term);
                if (!is_wp_error($term_link)) {
                    ?>
                    <li class="tgb-brands-item"><a href="<?php echo esc_url($term_link); ?>"><?php echo esc_html($term->name); ?></a></li>
                    <?php
                } else {
                    ?>
                    <li class="tgb-brands-item"><?php echo esc_html($term->name); ?></li>
                    <?php
                }
            }
            ?>
        </ul>
		<div class="letter-filter__letters">
            <span class="active" data-letter="A">A</span>
            <span data-letter="B">B</span>
            <span data-letter="C">C</span>
            <span data-letter="D">D</span>
            <span data-letter="E">E</span>
            <span data-letter="F">F</span>
            <span data-letter="G">G</span>
            <span data-letter="H">H</span>
            <span data-letter="I">I</span>
            <span data-letter="J">J</span>
            <span data-letter="K">K</span>
            <span data-letter="L">L</span>
            <span data-letter="M">M</span>
            <span data-letter="N">N</span>
            <span data-letter="O">O</span>
            <span data-letter="P">P</span>
            <span data-letter="Q">Q</span>
            <span data-letter="R">R</span>
            <span data-letter="S">S</span>
            <span data-letter="T">T</span>
            <span data-letter="U">U</span>
            <span data-letter="V">V</span>
            <span data-letter="W">W</span>
            <span data-letter="X">X</span>
            <span data-letter="Y">Y</span>
            <span data-letter="Z">Z</span>
        </div>
        </div>
        <?php
        $output = ob_get_clean(); // Get the buffer content and clean (end output buffering)
        
        $output .= '<style>
            
        </style>';
        
        $output .= '<script>
            jQuery(function($) {
                var Letter_Filter = {
                    config: {},
                    init: function(options) {
                        var self = this;
                        $.extend(this.config, options);
                        $(document).ready(function() {
                            self.initUI();
                        });
                    },
                    initUI: function() {
                        var self = this;
                        $(".letter-filter__letters span").on("click", function() {
							var letter = $(this).data("letter");

                            $(".letter-filter__letters span").removeClass("active");
							$(".letter-filter__letters span[data-letter=\"" + letter + "\"]").addClass("active");

                            $(this).addClass("active");
                            self.filterList($(this).text()[0]);
                        });
                        self.filterList("a");
                        return self;
                    },
                    filterList: function(character) {
                        $(".tgb-brands-container  li").show();
                        $(".tgb-brands-container  li").each(function() {
                            if ($(this).text()[0].toLowerCase() != character.toLowerCase()) {
                                $(this).hide();
                            }
                        });
                        return self;
                    }
                };

                Letter_Filter.init(); // Khởi tạo đúng
            });
        </script>';
        
        return $output;
    }
});


add_shortcode('shop_by_promotion', function () {
    $terms = get_terms(array(
        'taxonomy' => 'promotion',
        'hide_empty' => false,
    ));
	$output = '';
    if (!empty($terms) && !is_wp_error($terms)) {
        foreach ($terms as $term) {
				$banner = get_term_meta($term->term_id, 'banner', true);
			
				$term_link = get_term_link($term); 
				$output .= '<a href="' . esc_url($term_link) . '">';
				if (!empty($banner)) {
					$custom_field_value_url = wp_get_attachment_url($banner);
					$output .= '<img src="' . esc_url($custom_field_value_url) . '" alt="' . esc_attr($term->name) . '" class="custom-banner" style="width: 100%" />';
				}
			
				if (!is_wp_error($term_link)) {
					$output .= '<div class="shop_by_promotion_content_container"><h2 class="shop_by_promotion_title">' . esc_html($term->name) . '</h2><p class="shop_by_promotion_content">' . esc_html($term->description) . '</p></div>';
				}
				$output .= '</a>';
        }
        return $output;
    }
});
add_shortcode('brand_loop', function () {
    $brands = array();

    if (is_product_category()) {
        $category = get_queried_object();
        $category_id = $category->term_id;

        $category_posts_args = array(
            'post_type' => 'product', 
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'term_id',
                    'terms' => $category_id,
                ),
            ),
        );

        $category_posts = get_posts($category_posts_args);

        foreach ($category_posts as $product_post) {
            $product_brands = wp_get_post_terms($product_post->ID, 'brand');
    
            foreach ($product_brands as $brand_item) {
                $brands[$brand_item->term_id] = $brand_item;
            }
        }
    } else {
        $brands = get_terms(array(
            'taxonomy' => 'brand',
            'hide_empty' => false,
        ));
    }

    if (!empty($brands)) {
		echo '<ul class="brand-loop">';
		foreach ($brands as $brand) {
			$category_link = get_term_link($brand, 'brand');
			$brand_image = get_term_meta($brand->term_id, 'image', true);

			
			if (!empty($brand_image)) {
				echo '<li class=""> <a href="' . esc_url($category_link) . '">';
				echo wp_get_attachment_image($brand_image, 'full', false, array('alt' => esc_attr($brand->name), 'class' => 'custom-banner'));
				echo '</a></li>';
			} 

		}
		echo '</ul>';
	} else {
        ?>
        <script>
            $('.tgb-brands-title-container').hide();
        </script>
        <?php
    }

});


add_shortcode('group_buy_type_list', function () {
    $current_category = get_queried_object();
    $current_category_id = $current_category->term_id;

    $args = array(
        'taxonomy' => 'group-buy-category',
        'hide_empty' => false,
    );
    $categories = get_categories($args);

    
    ob_start();

    $categories_with_posts = array_filter($categories, function ($category) {
        $category_posts_args = array(
            'post_type' => 'group-buy', 
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'group-buy-category',
                    'field' => 'term_id',
                    'terms' => $category->term_id,
                ),
            ),
        );

        $category_posts = get_posts($category_posts_args);
        return !empty($category_posts);
    });

    if ($categories_with_posts) {
        echo '<ul class="cate_ul">';
        foreach ($categories_with_posts as $category) {
            $category_link = get_term_link($category, 'group-buy-category');
            $active_class = ($current_category_id == $category->term_id) ? 'active' : '';

            echo '<li class="' . esc_attr($active_class) . '">
				<a class="category-list-custom-item-url" href="#">' . esc_html($category->name) . '</a>';
            $category_posts_args = array(
                'post_type' => 'group-buy', 
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'group-buy-category',
                        'field' => 'term_id',
                        'terms' => $category->term_id,
                    ),
                ),
            ); 

            $category_posts = get_posts($category_posts_args);

            if ($category_posts) {
				echo '<ul class="post_ul">';
				foreach ($category_posts as $post) {
					$end_date_string = esc_html(get_post_meta($post->ID, 'end_date_time', true));

					$end_date = DateTime::createFromFormat('Ymd', $end_date_string);
					$formatted_end_date = $end_date ? $end_date->format('d/m/y') : '';

					$today = new DateTime();
					$three_days_later = clone $today;
					$three_days_later->modify('+3 days');
					$is_expiry = $end_date && $end_date < $three_days_later;
					$is_expired = $end_date && $end_date < $today;

					$row_class = $is_expired ? 'expired' : ($is_expiry ? 'expiry' : '');

					if(!$is_expired){
						echo '<li><div class="job-position-row">
							<a class="job-position-row-link" href="' . esc_url(get_permalink($post->ID)) . '">
								<span class="job-position-name">' . esc_html($post->post_title) . '</span>
								<span class="formatted_end_date ' . esc_attr($row_class) . '">Closing ' . ($is_expiry ? 'Soon' : 'Date') . ': ' . esc_html($formatted_end_date) . '</span>
							</a>
							<a class="job-position-row-detail-link elementor-button" href="' . esc_url(get_permalink($post->ID)) . '">Register</a>
						</div></li>';
					}
				}
				echo '</ul>';
			}




            echo '</li>';
        }
        echo '</ul>';
    } else {
        echo 'No categories found with posts.';
    }
    return ob_get_clean();
});

add_shortcode('countdown_timer', function () {
    ob_start(); 
    ?>
    <div id="countdown-container">
        <div class="countdown-box" id="days-box">
            <span class="countdown-value" id="days-value"></span>
            <span class="countdown-label">days</span>
        </div>
        <div class="countdown-box" id="hours-box">
            <span class="countdown-value" id="hours-value"></span>
            <span class="countdown-label">hrs</span>
        </div>
        <div class="countdown-box" id="minutes-box">
            <span class="countdown-value" id="minutes-value"></span>
            <span class="countdown-label">min</span>
        </div>
        <div class="countdown-box" id="seconds-box">
            <span class="countdown-value" id="seconds-value"></span>
            <span class="countdown-label">sec</span>
        </div>
    </div>

    <script>
    document.addEventListener("DOMContentLoaded", function() {
        var endDateTime = "<?php echo esc_html(get_post_meta(get_the_ID(), 'end_date_time', true)); ?>";

        if (endDateTime.length === 8) {
            endDateTime = endDateTime.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3 00:00:00');
        }

        var endDate = new Date(endDateTime);

        if (isNaN(endDate.getTime())) {
            console.error("Invalid date format for end_date_time:", endDateTime);
            document.getElementById("countdown-container").innerHTML = "Invalid date format";
            return;
        }

        var countDownDate = endDate.getTime();

        var x = setInterval(function() {
            var now = new Date().getTime();

            var distance = countDownDate - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            document.getElementById("days-value").innerHTML = days;
            document.getElementById("hours-value").innerHTML = hours;
            document.getElementById("minutes-value").innerHTML = minutes;
            document.getElementById("seconds-value").innerHTML = seconds;

            if (distance < 0) {
                clearInterval(x);
                document.getElementById("countdown-container").innerHTML = "EXPIRED";
            }
        }, 1000);
    });
    </script>
    <?php
    $output = ob_get_clean(); 
    return $output;
});

add_shortcode('show_pop_up', function () {
    
});



if(!is_admin()) {
	function wbp_search_filter($query) {
		if($query->is_search && !is_page(6009)) {
			$query->set('post_type','product');
		}
		return $query;
	}
	add_filter('pre_get_posts','wbp_search_filter');
}


// Hook into pre_get_posts to modify the query
add_action('pre_get_posts', 'modify_query_show_on_sale_products', 9999);

function modify_query_show_on_sale_products($query) {
    if (!is_admin() && $query->is_main_query() && $query->query_vars['wc_query'] == 'product_query' && is_shop() ){
        $query->set('meta_key', '_sale_price');
        $query->set('meta_value', '0');
        $query->set('meta_compare', '>');
    }
}
add_shortcode('tgb_get_product_count_message_custom', function () {
	?>
        <script>
            $(document).ready(function(){
// 				$( document ).on( 'submit', '.awf-filters-form', function() {
// 					console.log('submitted');
// 					if ($('.tgb-filter-result').length === 0) {
// 						$('.awf-filters-form').append('<div class="tgb-filter-result"></div>');
// 					}

// 					$('.tgb-filter-result').text($('p.woocommerce-result-count').text());
// 					$('p.woocommerce-result-count').hide();
// 				});
            });
        </script>

	<?php
});

add_shortcode('tgb_get_product_cat_custom_page', function () {
    $cate = get_queried_object();
    $cateID = $cate->term_id;
    $parent_categories = get_categories(array(
        'taxonomy' => 'product_cat',
        'parent' => $cateID,
        'hide_empty' => 0,
    ));

    if (empty($parent_categories)) {
        echo do_shortcode('[elementor-template id="7036"]');
        echo do_shortcode('[elementor-template id="7039"]');
		?>
		<script>
			$(document).ready(function(){
				$('.hide-on-child').hide();
			});
		</script>
		<?php
    }

    if (!empty($parent_categories)) {
        $parent_img_id = get_term_meta($cateID, 'thumbnail_id', true);
        $render = '<div class="tgb_cat_container">';

        foreach ($parent_categories as $index => $parent_category) {
            $term_id = $parent_category->term_id;

            $arr_color = array("#FFECB7", "#B7C5C5", "#B7D8FF", "#FFC1B7");
            $parent_category->bg_color = ($index % 2 !== 0) ? $arr_color[$index / 2] : "#F6F6F6";
            
            // $custom_field_name = 'background_color';
            // $term_id = $parent_category->term_id;
            // $custom_field_value = get_term_meta($term_id, $custom_field_name, true);
            // $parent_category->bg_color = !empty($custom_field_value) ? $custom_field_value : '#F6F6F6';
            
            // Get the thumbnail ID
            $thumbnail_id = get_term_meta($term_id, 'thumbnail_id', true);

            // Get the thumbnail URL
            $thumbnail_url = !empty($thumbnail_id) ? wp_get_attachment_image_src($thumbnail_id, 'thumbnail') : wp_get_attachment_image_src($parent_img_id, 'thumbnail');
            $category_link = get_term_link($parent_category, 'product_cat');

            if (is_wp_error($category_link)) {
                continue;
            }

            // Check if level 2 category doesn't have an image
            if (empty($thumbnail_id) && $cate->parent != 0) {
                // Get the parent category's thumbnail ID
                $parent_thumbnail_id = get_term_meta($cate->parent, 'thumbnail_id', true);

                // Get the parent category's thumbnail URL
                $parent_thumbnail_url = !empty($parent_thumbnail_id) ? wp_get_attachment_image_src($parent_thumbnail_id, 'thumbnail') : '';

                // Use the parent category's thumbnail if available
                if (!empty($parent_thumbnail_url)) {
                    $thumbnail_url = $parent_thumbnail_url;
                }
            }

            $render .= '<div class="tgb_cat_detail" style="padding: 0;">';
            $render .= '<div class="tgb_cat">';
            $render .= '<div class="tgb_cat_" style="background-color: ' . $parent_category->bg_color . '">';
            $render .= '<div class="tgb_cat_name"><a class="" href="' . esc_url($category_link) . '">' . $parent_category->name . '</a></div>';
            $render .= '<div class="tgb_cat_link">View <a class="tgb_cat_url" href="' . esc_url($category_link) . '">more</a></div>';
            $render .= '<img src="' . $thumbnail_url[0] . '" alt="' . $parent_category->name . '">';
            $render .= '</div>';
            $render .= '</div>';
            $render .= '</div>';
        }

        $render .= '</div>';
        echo $render;
    }
});


add_filter( 'woocommerce_breadcrumb_defaults', function($defaults){
    // Change the breadcrumb delimiter from '/' to '>'
    $defaults['delimiter'] = ' > ';
	$defaults['home']      = _x( 'Unidbox', 'breadcrumb', 'woocommerce' );
    return $defaults;
} );

add_shortcode('job_postions_type_list', function(){
    $current_category = get_queried_object();
    $current_category_id = $current_category->term_id;

    // Get all categories of custom taxonomy 'services_category'
    $args = array(
        'taxonomy' => 'job-positions-category',
        'hide_empty' => false, // Show even empty categories
    );
    $categories = get_categories($args);

    // Start output buffer
    ob_start();

    // Check if there are any categories with posts
    $categories_with_posts = array_filter($categories, function($category) {
        $category_posts_args = array(
            'post_type' => 'job-position', // replace with your custom post type
            'posts_per_page' => 1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'job-positions-category',
                    'field' => 'term_id',
                    'terms' => $category->term_id,
                ),
            ),
        );
        $category_posts = get_posts($category_posts_args);
        return !empty($category_posts);
    });

    if ($categories_with_posts) {
        echo '<ul class="cate_ul">';
        foreach ($categories_with_posts as $category) {
            $category_link = get_term_link($category, 'job-positions-category');
            // Check if the category is the current one
            $active_class = ($current_category_id == $category->term_id) ? 'active' : '';

            // Output the category with the active class
            echo '<li class="' . esc_attr($active_class) . '">
                <a class="category-list-custom-item-url">' . esc_html($category->name) . '</a>';

            // Fetch and display posts for the current category
            $category_posts_args = array(
                'post_type' => 'job-position', // replace with your custom post type
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'job-positions-category',
                        'field' => 'term_id',
                        'terms' => $category->term_id,
                    ),
                ),
            );
            $category_posts = get_posts($category_posts_args);

            if ($category_posts) {
				echo '<ul class="post_ul">';
				foreach ($category_posts as $post) {
					echo '<li><div class="job-position-row"><a class="job-position-row-link" href="' . esc_url(get_permalink($post->ID)) . '"><span>' . esc_html($post->post_title) . '</span><span>' . esc_html($post->type) . '</span><span><svg xmlns="http://www.w3.org/2000/svg" width="11" height="16" viewBox="0 0 11 16" fill="none">
<path d="M5.5 0.5C2.45929 0.5 0 2.8475 0 5.75C0 9.6875 5.5 15.5 5.5 15.5C5.5 15.5 11 9.6875 11 5.75C11 2.8475 8.54071 0.5 5.5 0.5ZM5.5 7.625C4.41571 7.625 3.53571 6.785 3.53571 5.75C3.53571 4.715 4.41571 3.875 5.5 3.875C6.58429 3.875 7.46429 4.715 7.46429 5.75C7.46429 6.785 6.58429 7.625 5.5 7.625Z" fill="#253D4E"/>
</svg>&nbsp;&nbsp;' . esc_html($post->address) . '</span></a><a class="job-position-row-detail-link elementor-button" href="' . esc_url(get_permalink($post->ID)) . '">Find out more</a></div></li>';
				}
				echo '</ul>';
			}


            echo '</li>';
        }
        echo '</ul>';
    } else {
        echo 'No categories found with posts.';
    }

    // End output buffer and return content
    return ob_get_clean();
});

add_shortcode('post_days_ago', function($atts){
    $atts = shortcode_atts(array(
        'id' => get_the_ID(),
    ), $atts, 'post_days_ago');

    $post_date = get_the_time('U', $atts['id']);
    $current_date = current_time('timestamp');
    $days_ago = 'Posted ' . ucwords(human_time_diff($post_date, $current_date)) . ' ago';

    return $days_ago;
});
add_action('template_redirect', 'woocommerce_clear_cart_url');

function woocommerce_clear_cart_url() {
    global $woocommerce;

    // Check if it's the cart page and 'empty-cart' parameter is set
    if ((is_page('cart') || is_cart()) && isset($_GET['empty-cart'])) {
        $woocommerce->cart->empty_cart();
		wp_redirect(get_permalink(wc_get_page_id('shop')));
        exit();
    }
}

add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields11' );
function custom_override_default_address_fields11( $address_fields ) {
    $address_fields['address_2']['placeholder'] = 'Unit Number';
    $address_fields['address_2']['label'] = 'Unit Number';
	$address_fields['phone']['placeholder'] = 'Contact Number';
    $address_fields['phone']['label'] = 'Contact Number';
    $address_fields['phone']['label'] = 'Contact Number';
	$address_fields['postcode']['label'] = 'Postal Code';
	$address_fields['postcode']['label'] = 'Postal Code';
    return $address_fields;
}

add_filter( 'woocommerce_cart_shipping_method_full_label', 'bbloomer_add_0_to_shipping_label', 9999, 2 );
 
function bbloomer_add_0_to_shipping_label( $label, $method ) {
   if ( ! ( $method->cost > 0 ) ) {
      $label .= wc_price( 0 );
   }
   return $label;
}
 
add_filter( 'woocommerce_order_shipping_to_display', 'bbloomer_add_0_to_shipping_label_ordered', 9999, 3 );
 
function bbloomer_add_0_to_shipping_label_ordered( $shipping, $order, $tax_display ) {
   if ( ! ( 0 < abs( (float) $order->get_shipping_total() ) ) && $order->get_shipping_method() ) {
      $shipping .= wc_price( 0 );
   }
   return $shipping;
}

add_filter( 'woocommerce_pagination_args', 	'tgb_custom_pagination' );
function tgb_custom_pagination( $args ) {

	$args['prev_text'] = '<i class="fa fa-angle-left"></i>';
	$args['next_text'] = '<i class="fa fa-angle-right"></i>';

	return $args;
}

if( !function_exists( 'custom_login_logo' ) ){
    function custom_login_logo() {
        echo '<style>
            h1 a { 
				background-image: url("'.esc_url( wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' )[0] ).'") !important; 
                width: 100% !important;
                background-size: unset !important;
                min-height: 50px;
                height: unset !important;
            }
            #nsl-custom-login-form-main {
                display: none;
            }
        </style>';
    }
    add_action( 'login_head', 'custom_login_logo' );
}

add_shortcode('redirect_after_5s', function($atts){
	?>
<script>
	setTimeout(function(){
		window.location.href = '<?= site_url(); ?>';
	}, 5000)
</script>
	<?php

});
    function verify_recaptcha_on_woocommerce_registration($username, $email, $errors) {
    if (empty($_POST['g-recaptcha-response'])) {
        $errors->add('error', __('Please complete the reCAPTCHA to register.', 'woocommerce'));
    } else {
        $recaptcha_secret = '6LeO1lAnAAAAABw7rJeIXK2uM7ORcLTwBfHde3dh';
        $recaptcha_response = sanitize_text_field($_POST['g-recaptcha-response']);
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_data = array(
            'secret' => $recaptcha_secret,
            'response' => $recaptcha_response,
            'remoteip' => $_SERVER['REMOTE_ADDR'],
        );
        $recaptcha_options = array(
            'body' => $recaptcha_data,
        );

        $response = wp_remote_post($recaptcha_url, $recaptcha_options);
        $recaptcha_result = json_decode(wp_remote_retrieve_body($response));

        if (!$recaptcha_result->success) {
            $errors->add('error', __('reCAPTCHA verification failed. Please try again.', 'woocommerce'));
        }
    }
}
function verify_recaptcha_on_woocommerce_login($username, $password, $errors) {
    if (empty($_POST['g-recaptcha-response'])) {
        $errors->add('error', __('Please complete the reCAPTCHA to register.', 'woocommerce'));
    } else {
        $recaptcha_secret = '6LeO1lAnAAAAABw7rJeIXK2uM7ORcLTwBfHde3dh';
        $recaptcha_response = sanitize_text_field($_POST['g-recaptcha-response']);
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_data = array(
            'secret' => $recaptcha_secret,
            'response' => $recaptcha_response,
            'remoteip' => $_SERVER['REMOTE_ADDR'],
        );
        $recaptcha_options = array(
            'body' => $recaptcha_data,
        );

        $response = wp_remote_post($recaptcha_url, $recaptcha_options);
        $recaptcha_result = json_decode(wp_remote_retrieve_body($response));

        if (!$recaptcha_result->success) {
            $errors->add('error', __('reCAPTCHA verification failed. Please try again.', 'woocommerce'));
        }
    }
}
function verify_recaptcha_on_woocommerce_lost_password($errors) {
    if (empty($_POST['g-recaptcha-response'])) {
        $errors->add('error', __('Please complete the reCAPTCHA to register.', 'woocommerce'));
    } else {
        $recaptcha_secret = '6LeO1lAnAAAAABw7rJeIXK2uM7ORcLTwBfHde3dh';
        $recaptcha_response = sanitize_text_field($_POST['g-recaptcha-response']);
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_data = array(
            'secret' => $recaptcha_secret,
            'response' => $recaptcha_response,
            'remoteip' => $_SERVER['REMOTE_ADDR'],
        );
        $recaptcha_options = array(
            'body' => $recaptcha_data,
        );

        $response = wp_remote_post($recaptcha_url, $recaptcha_options);
        $recaptcha_result = json_decode(wp_remote_retrieve_body($response));

        if (!$recaptcha_result->success) {
            $errors->add('error', __('reCAPTCHA verification failed. Please try again.', 'woocommerce'));
        }
    }
}
add_action('woocommerce_register_post', 'verify_recaptcha_on_woocommerce_registration', 10, 3);
add_action('woocommerce_authenticate_user', 'verify_recaptcha_on_woocommerce_login', 10, 3);
add_action('lostpassword_post', 'verify_recaptcha_on_woocommerce_lost_password');

add_shortcode('custom_annasta_filter', function(){
    ?>
    <script>
    jQuery( document ).on( 'awf_after_setup', function() { 
        jQuery( '.awf-filters-form .awf-filter, .awf-filters-form .awf-apply-filter-btn' ).on( 'click', function() { 
            jQuery( this ).closest( '.awf-filter-wrapper' ).toggleClass('awf-collapsed ',' '); 
        } ); 
    } );
    </script>
    <?php
});
add_action( 'elementor_pro/forms/validation', 'tgb_elementor_forms_validation', 10, 2 );
// This function allows to obtain a field by ID, if it does not exist it returns FALSE.
function tgb_elementor_get_field( $id, $record )
{
    $fields = $record->get_field( [
        'id' => $id,
    ] );

    if ( empty( $fields ) ) {
        return false;
    }

    return current( $fields );
}
function tgb_elementor_forms_validation( $record, $ajax_handler ) {
    // [a-zA-Z][a-zA-Z ]{0,255}
    if( $field = tgb_elementor_get_field( 'fname', $record ) )
    {
        if( $field['required'] == 'yes' ){
            if( empty( trim($field['value']) ) ){ // Check if it is empty
                $ajax_handler->add_error( $field['id'], 'Please input first name.' );
            } else if( !preg_match( '/^[a-zA-Z][a-zA-Z ]{0,255}$/', trim($field['value']) ) ) {
                $ajax_handler->add_error( $field['id'], 'Please input valid first name.' );
            }
        }
    }
    if( $field = tgb_elementor_get_field( 'lname', $record ) )
    {
        if( $field['required'] == 'yes' ){
            if( empty( trim($field['value']) ) ){ // Check if it is empty
                $ajax_handler->add_error( $field['id'], 'Please input last name.' );
            } else if( !preg_match( '/^[a-zA-Z][a-zA-Z ]{0,255}$/', trim($field['value']) ) ) {
                $ajax_handler->add_error( $field['id'], 'Please input valid last name.' );
            }
        }
    }
    if( $field = tgb_elementor_get_field( 'phone', $record ) )
    {
        if( $field['required'] == 'yes' ){
            if( empty( trim($field['value']) ) ){ // Check if it is empty
                $ajax_handler->add_error( $field['id'], 'Please input phone.' );
            } else if( !preg_match( '/^[0-9]{8,20}$/', trim($field['value']) ) ) {
                $ajax_handler->add_error( $field['id'], 'Please input valid phone.' );
            }
        }
    }
}

function wpf_dev_disallow_numbers_text_field( $fields, $entry, $form_data ) {
         
    
    if ( absint( $form_data[ 'id' ] ) !== 6170 ) {
        return $fields;
    }

    $fname = $fields[13][ 'value' ];
    $lname = $fields[14][ 'value' ];
  
    if (!preg_match ('/^[a-zA-Z][a-zA-Z ]{0,255}$/', $fname))  {
        wpforms()->process->errors[ $form_data[ 'id' ] ] [ '13' ] = esc_html__( 'The First Name can only contain letters.', 'plugin-domain' );
    }

    if (!preg_match ('/^[a-zA-Z][a-zA-Z ]{0,255}$/', $lname))  {
        wpforms()->process->errors[ $form_data[ 'id' ] ] [ '14' ] = esc_html__( 'The Last Name can only contain letters.', 'plugin-domain' );
    }
     
}
  
add_action( 'wpforms_process', 'wpf_dev_disallow_numbers_text_field', 10, 3 );

?>
