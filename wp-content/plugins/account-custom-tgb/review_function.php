<?php
add_shortcode('product_reviews_list', function(){
    global $product;
    $product_id = $product->get_id();
    $reviews = get_comments(array(
        'post_id' => $product_id,
        'status' => 'approve', 
    ));
    ob_start();
    ?>
    <ul class="product-reviews">
        <?php foreach ($reviews as $review) : ?>
            <li class="product-review">
                <?php
                    $rating = get_comment_meta($review->comment_ID, 'rating', true);
                    $star_active = '<i aria-hidden="true" class="fas fa-star"></i>';
                    $star_ = '<i aria-hidden="true" class="far fa-star"></i>';
                    $render_rating = '<div class="custom-star-rating">';
                    for ($i = 1; $i <= 5; $i++) {
                        if ($i <= ceil($rating)) {
                            $render_rating .= $star_active;
                        } else {
                            $render_rating .= $star_;
                        }
                    }
                    $render_rating .= '</div>'; 
                ?>
                <div class="review-rating"><?php echo $render_rating; ?></div>
                <div class="review-author"><?php echo esc_html($review->comment_author); ?></div>
                <div class="review-date"><?php echo esc_html(date('Y-m-d | H:i', strtotime($review->comment_date))); ?></div>
                <div class="review-content"><?php echo esc_html($review->comment_content); ?></div>
                <?php 
				$review_attachment = get_comment_meta($review->comment_ID, 'review_attachment', true);
                if ($review_attachment) : ?>
                    <div class="review-attachment-view">
                        <img src="<?php echo home_url().'/wp-content/uploads'. $review_attachment; ?>" alt="Review Attachment" height="200" width="200">
                    </div>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php
    return ob_get_clean();
});

add_action('comment_post', function($comment_ID, $comment_approved){
    if (!empty($_FILES['file-input-review-pr']['name'])) {
        $uploaded_file = $_FILES['file-input-review-pr'];
        $upload_dir = wp_upload_dir();
        $upload_path = $upload_dir['basedir'] . '/reviews/';
    
        if (!file_exists($upload_path)) {
            wp_mkdir_p($upload_path);
        }
    
        $thumbnail_size = array(100, 100);
        $file_name = sanitize_file_name(basename($uploaded_file['name']));
        $new_file_path = $upload_path . $file_name;
    
        $file_data = wp_handle_upload($uploaded_file, array('test_form' => false, 'upload_path' => $upload_path));
    
        if (!isset($file_data['error'])) {
            $resized_image = wp_get_image_editor($file_data['file']);
    
            if (!is_wp_error($resized_image)) {
                $resized_image->resize($thumbnail_size[0], $thumbnail_size[1], true);
                $resized_image->save($new_file_path);
            }
    
            // Save the file path in the desired format
            $relative_file_path = str_replace($upload_dir['basedir'], '', $new_file_path);
            add_comment_meta($comment_ID, 'review_attachment', $relative_file_path);
        }
    }
    
}, 10, 2);