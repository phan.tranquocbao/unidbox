<?php
add_shortcode('login_button_custom', function (){
	$icon = '<svg xmlns="http://www.w3.org/2000/svg" width="33" height="32" viewBox="0 0 33 32" fill="none"><path d="M16.667 27.52C12.667 27.52 9.13099 25.472 7.06699 22.4C7.11499 19.2 13.467 17.44 16.667 17.44C19.867 17.44 26.219 19.2 26.267 22.4C25.2092 23.9751 23.7804 25.2658 22.1063 26.1587C20.4322 27.0515 18.5643 27.519 16.667 27.52ZM16.667 4.8C17.94 4.8 19.1609 5.30571 20.0611 6.20589C20.9613 7.10606 21.467 8.32696 21.467 9.6C21.467 10.873 20.9613 12.0939 20.0611 12.9941C19.1609 13.8943 17.94 14.4 16.667 14.4C15.394 14.4 14.1731 13.8943 13.2729 12.9941C12.3727 12.0939 11.867 10.873 11.867 9.6C11.867 8.32696 12.3727 7.10606 13.2729 6.20589C14.1731 5.30571 15.394 4.8 16.667 4.8ZM16.667 0C14.5658 0 12.4853 0.413852 10.5441 1.21793C8.60285 2.022 6.83902 3.20055 5.35328 4.68629C2.3527 7.68687 0.666992 11.7565 0.666992 16C0.666992 20.2435 2.3527 24.3131 5.35328 27.3137C6.83902 28.7994 8.60285 29.978 10.5441 30.7821C12.4853 31.5862 14.5658 32 16.667 32C20.9105 32 24.9801 30.3143 27.9807 27.3137C30.9813 24.3131 32.667 20.2435 32.667 16C32.667 7.152 25.467 0 16.667 0Z" fill="#253D4E"></path></svg>';
    $url = "#link-popup-account";
    if(is_user_logged_in()){
        $url = home_url().'/my-account';
    }
    $button = '<div class="elementor-element elementor-view-default elementor-widget elementor-widget-icon" data-element_type="widget" data-widget_type="icon.default">
		<div class="elementor-widget-container">
			<div class="elementor-icon-wrapper">
                <a class="elementor-icon" href="'.$url.'">
                    '.$icon.'
                </a>
		    </div>
		</div>
	</div>';
	return $button;
});

add_action('woocommerce_created_customer', function($customer_id){
    if (isset($_POST['billing_first_name'])) {
        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']));
    }
    if (isset($_POST['billing_last_name'])) {
        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']));
    }
    if (isset($_POST['billing_phone'])) {
        update_user_meta($customer_id, 'billing_phone', sanitize_text_field($_POST['billing_phone']));
    }
    if (isset($_POST['billing_postcode'])) {
        update_user_meta($customer_id, 'billing_postcode', sanitize_text_field($_POST['billing_postcode']));
    }
    if (isset($_POST['billing_address_1'])) {
        update_user_meta($customer_id, 'billing_address_1', sanitize_text_field($_POST['billing_address_1']));
    }
	update_user_meta($customer_id, 'billing_country', 'SG');
	update_user_meta($customer_id, 'shipping_country', 'SG');
});

add_filter( 'woocommerce_process_registration_errors', function($validation_error, $password, $email){
    if ( isset( $_POST['confirm_password'] ) && $_POST['confirm_password'] !== $_POST['password'] ) {
        $validation_error->add('error', 'Confirm password does not match');
    }
    if ( isset( $_POST['email'] ) ) {
        $email = trim( $_POST['email'] );
    
        if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
            $validation_error->add( 'error', 'Invalid email address format' );
        }
    }
    if (isset($_POST['billing_phone'])) {
        $phone = $_POST['billing_phone'];
        if (!preg_match('/^[0-9]+$/', $phone)) {
            $validation_error->add('error', 'Phone number should only contain numeric characters');
        }
    }
    return $validation_error;
}, 10, 4 );


add_filter('woocommerce_checkout_fields', function ($fields){
    // Unset the 'required' attribute for shipping fields
    unset($fields['shipping']['shipping_country']['required']);
    unset($fields['shipping']['shipping_first_name']['required']);
    unset($fields['shipping']['shipping_last_name']['required']);
    unset($fields['shipping']['shipping_address_1']['required']);
	unset($fields['shipping']['shipping_address_2']['required']);
    unset($fields['shipping']['shipping_city']['required']);
    unset($fields['shipping']['shipping_postcode']['required']);
    unset($fields['shipping']['shipping_state']['required']);

    return $fields;
});


add_action('woocommerce_checkout_update_order_meta', function ($order_id){
    $ship_to_different_address = isset($_POST['ship_to_different_address']) ? wc_clean($_POST['ship_to_different_address']) : 0;
	// Lấy thông tin địa chỉ thanh toán
	$billing_first_name = sanitize_text_field($_POST['billing_first_name']);
	$billing_last_name = sanitize_text_field($_POST['billing_last_name']);
	$billing_phone = sanitize_text_field($_POST['billing_phone']);
	$billing_address_1 = sanitize_text_field($_POST['billing_address_1']);
	$billing_address_2 = sanitize_text_field($_POST['billing_address_2']);
	$billing_city = sanitize_text_field($_POST['billing_city']);
	$billing_postcode = sanitize_text_field($_POST['billing_postcode']);
	$billing_country = sanitize_text_field($_POST['billing_country']);
	$billing_state = sanitize_text_field($_POST['billing_state']);
	
    if ($ship_to_different_address == '1') {
        // Cập nhật thông tin địa chỉ giao hàng
        update_post_meta($order_id, '_shipping_first_name', $billing_first_name);
        update_post_meta($order_id, '_shipping_last_name', $billing_last_name);
        update_post_meta($order_id, '_shipping_address_1', $billing_address_1);
        update_post_meta($order_id, '_shipping_address_2', $billing_address_2);
        update_post_meta($order_id, '_shipping_city', $billing_city);
		update_post_meta($order_id, '_shipping_phone', $billing_phone);
        update_post_meta($order_id, '_shipping_postcode', $billing_postcode);
        update_post_meta($order_id, '_shipping_country', $billing_country);
        update_post_meta($order_id, '_shipping_state', $billing_state);
    }else{
		$shipping_first_name = sanitize_text_field($_POST['shipping_first_name']);
        $shipping_last_name = sanitize_text_field($_POST['shipping_last_name']);
        $shipping_address_1 = sanitize_text_field($_POST['shipping_address_1']);
        $shipping_address_2 = sanitize_text_field($_POST['shipping_address_2']);
		$shipping_phone = sanitize_text_field($_POST['shipping_phone']);
        $shipping_city = sanitize_text_field($_POST['shipping_city']);
        $shipping_postcode = sanitize_text_field($_POST['shipping_postcode']);
        $shipping_country = sanitize_text_field($_POST['shipping_country']);
        $shipping_state = sanitize_text_field($_POST['shipping_state']);
		
		if ($shipping_first_name !== "" && $shipping_first_name !== null){
			update_post_meta($order_id, '_shipping_first_name', $shipping_first_name);
		}else{
			update_post_meta($order_id, '_shipping_first_name', $billing_first_name);
		}
		
		if ($shipping_last_name !== "" && $shipping_last_name !== null){
			update_post_meta($order_id, '_shipping_last_name', $shipping_last_name);
		}else{
			update_post_meta($order_id, '_shipping_last_name', $billing_last_name);
		}
		
		if ($shipping_address_1 !== "" && $shipping_address_1 !== null){
			update_post_meta($order_id, '_shipping_address_1', $shipping_address_1);
		}else{
			update_post_meta($order_id, '_shipping_address_1', $billing_address_1);
		}
		
		if ($shipping_address_2 !== "" && $shipping_address_2 !== null){
			update_post_meta($order_id, '_shipping_address_2', $shipping_address_2);
		}else{
			update_post_meta($order_id, '_shipping_address_2', $billing_address_2);
		}
		if ($shipping_phone !== "" && $shipping_phone !== null){
			update_post_meta($order_id, '_shipping_phone', $shipping_phone);
		}else{
			update_post_meta($order_id, '_shipping_phone', $billing_phone);
		}
		if ($shipping_city !== "" && $shipping_city !== null){
			update_post_meta($order_id, '_shipping_city', $shipping_city);
		}else{
			update_post_meta($order_id, '_shipping_city', $billing_city);
		}
		
     	if ($shipping_postcode !== "" && $shipping_postcode !== null){
			update_post_meta($order_id, '_shipping_postcode', $shipping_postcode);
		}else{
			update_post_meta($order_id, '_shipping_postcode', $billing_postcode);
		}
		
		if ($shipping_country !== "" && $shipping_country !== null){
			update_post_meta($order_id, '_shipping_country', $shipping_country);
		}else{
			update_post_meta($order_id, '_shipping_country', $billing_country);
		}
    
    	if ($shipping_state !== "" && $shipping_state !== null){
			update_post_meta($order_id, '_shipping_state', $shipping_state);
		}else{
			update_post_meta($order_id, '_shipping_state', $billing_state);
		}
	}
});


add_action('woocommerce_after_checkout_validation', function($data, $errors){
	$ship_to_different_address = isset($_POST['ship_to_different_address']) ? wc_clean($_POST['ship_to_different_address']) : 0;
	$billing_phone = sanitize_text_field($_POST['billing_phone']);
	if ($billing_phone !== "" && $billing_phone !== null){
			$billing_phone_val = preg_replace('/\s+/', '', $billing_phone);

			if(strlen($billing_phone_val) <= 7 || strlen($billing_phone_val) > 20){
			$errors->add( 'validation', __( '<strong>Billing Phone</strong> length must be from 8 to 20 numberics.' ));

			}
		}
	if ($ship_to_different_address !== '1') {
		$shipping_first_name = sanitize_text_field($_POST['shipping_first_name']);
        $shipping_last_name = sanitize_text_field($_POST['shipping_last_name']);
        $shipping_address_1 = sanitize_text_field($_POST['shipping_address_1']);
        $shipping_address_2 = sanitize_text_field($_POST['shipping_address_2']);
		$shipping_phone = sanitize_text_field($_POST['shipping_phone']);
        $shipping_city = sanitize_text_field($_POST['shipping_city']);
        $shipping_postcode = sanitize_text_field($_POST['shipping_postcode']);
        $shipping_country = sanitize_text_field($_POST['shipping_country']);
        $shipping_state = sanitize_text_field($_POST['shipping_state']);
		
		if ($shipping_first_name == "" && $shipping_first_name == null){
			$errors->add( 'validation', __( '<strong>Shipping First Name</strong> is required!' ));
		}
		if ($shipping_last_name == "" && $shipping_last_name == null){
			$errors->add( 'validation', __( '<strong>Shipping First Name</strong> is required!' ));
		}
		if ($shipping_address_1 == "" && $shipping_address_1 == null){
			$errors->add( 'validation', __( '<strong>Street Address Shipping</strong> is required!' ));
		}
		if ($shipping_address_2 == "" && $shipping_address_2 == null){
			$errors->add( 'validation', __( '<strong>Unit Number Shipping</strong> is required!' ));
		}
		if ($shipping_phone == "" && $shipping_phone == null){
			$errors->add( 'validation', __( '<strong>Shipping Phone</strong> is required!' ));
		}
		if ($shipping_phone !== "" && $shipping_phone !== null){
			$shipping_phone_val = preg_replace('/\s+/', '', $shipping_phone);

			if(strlen($shipping_phone_val) <= 7 || strlen($shipping_phone_val) > 20){
			$errors->add( 'validation', __( '<strong>Shipping Phone</strong> length must be from 8 to 20 numberics.' ));

			}
		}
		if ($shipping_city == "" && $shipping_city == null){
			$errors->add( 'validation', __( '<strong>Shipping City</strong> is required!' ));
		}
		if ($shipping_postcode == "" && $shipping_postcode == null){
			$errors->add( 'validation', __( '<strong>Shipping Postal Code</strong> is required!' ));
		}
		if ($shipping_country == "" && $shipping_country == null){
			$errors->add( 'validation', __( '<strong>Shipping Country</strong> is required!' ));
		}
		if ($shipping_state == "" && $shipping_state == null){
			$errors->add( 'validation', __( '<strong>Shipping State/Country</strong> is required!' ));
		}
	}
},10,2);

add_filter( 'woocommerce_account_menu_items', function($items){
	 unset( $items['downloads'] );
    return $items;
}, 999 );
