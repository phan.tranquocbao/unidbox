<?php
/*
Plugin Name: Custom Account Functions Woocomerce
Description: Bona Technologies plugin.
Author: TGB
Version: 1.0.0
*/

// Add file function
require_once(plugin_dir_path(__FILE__) . 'cart_custom_function.php');
require_once(plugin_dir_path(__FILE__) . 'account_function.php');
require_once(plugin_dir_path(__FILE__) . 'products_function.php');
require_once(plugin_dir_path(__FILE__) . 'review_function.php');
function get_categories_recursive($parent_id = 0, $depth = 0)
{
    $categories = get_terms(array(
        'taxonomy' => 'product_cat',
        'parent' => $parent_id,
    ));

    if (empty($categories)) {
        return ''; // Kết thúc đệ quy nếu không có category con
    }

    $output = '<ul class="category-list-custom">';
    foreach ($categories as $category) {
        $category_link = get_term_link($category, 'product_cat');

        if (is_wp_error($category_link)) {
            continue;
        }
        // Nếu có subcategories, thêm dấu ">" và gọi đệ quy
        $subcategories = get_categories_recursive($category->term_id, $depth + 1);
        $output .= '<li class="category-list-custom-item" id="category_parent-' . $category->term_id . '">';
        $output .= '<a class="category-list-custom-item-url" href="' . esc_url($category_link) . '">' . esc_html($category->name) . '</a>';
        if ($subcategories) {
            $output .= '<a class="sub_item-ind" onclick="toggleSubcategories(this, ' . $category->term_id . ')">
                <i aria-hidden="true" class="eicon-chevron-right"></i>
            </a>';

            $output .= '<div id="subcategories-' . $category->term_id . '" class="subcategories-container" style="display:none;">';
            $output .= '<a class="back-button ' . ($depth === 0 ? 'child-cat' : '') . '" onclick="goBack(this, ' . $category->term_id . ')" id="back-cat-' . $category->term_id . '"><i aria-hidden="true" class="eicon-chevron-left"></i> ' . esc_html($category->name) . '</a>';
            $output .= $subcategories;
            $output .= '</div>';
        }
        $output .= '</li>';
    }
    $output .= '</ul>';

    return $output;
}
add_shortcode('parent_categories_list', function () {
    return get_categories_recursive();
});

// Get category service

add_shortcode('services_list', function () {
    $current_category = get_queried_object();
    $current_category_id = $current_category->term_id;

    // Get all categories of custom taxonomy 'services_category'
    $args = array(
        'taxonomy' => 'category_services',
        'hide_empty' => false, // Show even empty categories
    );
    $categories = get_categories($args);

    // Start output buffer
    ob_start();

    // Check if there are any categories
    if ($categories) {
        echo '<ul>';
        foreach ($categories as $category) {
            $category_link = get_term_link($category, 'category_services');
            // Check if the category is the current one
            $active_class = ($current_category_id == $category->term_id) ? 'active' : '';

            // Output the category with the active class
            echo '<li class="' . esc_attr($active_class) . '">
				<a class="category-list-custom-item-url" href="' . esc_url($category_link) . '">' . esc_html($category->name) . '</a>
			</li>';
        }
        echo '</ul>';
    } else {
        echo 'No categories found.';
    }

    // End output buffer and return content
    return ob_get_clean();
});


// add_filter( 'elementor_pro/dynamic_tags/shortcode/should_escape', '__return_false' ); 


add_shortcode('rating_and_sold_product', function () {
    global $product;
    $total_sold = !empty($product) ? $product->get_total_sales() : 0;
    $rating = !empty($product) ? $product->get_average_rating() : 0;
    $star_ = '<i aria-hidden="true" class="fas fa-star" style="color: var(--e-global-color-6c52abe);"></i>';
    $star_active = '<i aria-hidden="true" class="fas fa-star" style="color: var(--e-global-color-secondary);"></i>';
    $render_rating = '<div class="custom-star-rating">';
    for ($i = 1; $i <= 5; $i++) {
        if ($i <= ceil($rating)) {
            $render_rating .= $star_active;
        } else {
            $render_rating .= $star_;
        }
    }
    $render_rating .= '</div>';

    echo $render_rating . ' <span class"product-total-sold">' . number_format($rating, 1) . ' | ' . $total_sold . ' sold </span>';
});

add_shortcode('tgb_post_author_custom', function () {
    $author_id = get_the_author_meta('ID');
    $author_name = get_the_author_meta('display_name', $author_id);
    $author_avatar = get_avatar($author_id, 50);
    $post_date = get_the_date('jS F Y');

    $render = '<div class="tgb_post_info">
				<div class="tgb_post_author-avatar">' . $author_avatar . '</div>
				<div class="tgb_post_details">
					<div class="tgb_post_author-name">' . $author_name . '</div>
					<div class="tgb_post_date">Posted on ' . $post_date . '</div>
				</div>
			</div>';
    echo $render;
});

add_shortcode('tgb_get_product_cat_custom', function () {
    $parent_categories = get_categories(array(
        'taxonomy' => 'product_cat',
        'parent' => 0,
        'hide_empty' => 0,
        'number' => 8,
    ));

    $render = '<div class="tgb_cat_container">';

    foreach ($parent_categories as $index => $parent_category) {
        $term_id = $parent_category->term_id;
        $arr_color = array("#FFECB7", "#B7C5C5", "#B7D8FF", "#FFC1B7");
        $parent_category->bg_color = ($index % 2 !== 0) ? "#F6F6F6" : $arr_color[$index / 2];

        // $custom_field_name = 'background_color';
        // $term_id = $parent_category->term_id;
        // $custom_field_value = get_term_meta($term_id, $custom_field_name, true);
        // $parent_category->bg_color = !empty($custom_field_value) ? $custom_field_value : '#F6F6F6';

        // Get the thumbnail ID
        $thumbnail_id = get_term_meta($term_id, 'thumbnail_id', true);
        // Get the thumbnail URL
        $thumbnail_url = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');
        $category_link = get_term_link($parent_category, 'product_cat');

        if (is_wp_error($category_link)) {
            continue;
        }
        $render .= '<div class="tgb_cat_detail">';
        $render .= '<div class="tgb_cat_" style="background-color: ' . $parent_category->bg_color . '">';
        $render .= '<div class="tgb_cat_name">' . $parent_category->name . '</div>';
        $render .= '<div class="tgb_cat_link">View <a class="tgb_cat_url" href="' . esc_url($category_link) . '">more</a></div>';
        $render .= '<img src="' . $thumbnail_url[0] . '" alt="' . $parent_category->name . '">';
        $render .= '</div>';
        $render .= '</div>';
    }

    $render .= '</div>';
    echo $render;
});


add_filter('woocommerce_breadcrumb_defaults', function ($defaults) {
    // Change the breadcrumb delimiter from '/' to '>'
    $defaults['delimiter'] = ' > ';
    return $defaults;
});

add_shortcode('job_postions_type_list', function () {
    $current_category = get_queried_object();
    $current_category_id = $current_category->term_id;

    // Get all categories of custom taxonomy 'services_category'
    $args = array(
        'taxonomy' => 'job-positions-category',
        'hide_empty' => false, // Show even empty categories
    );
    $categories = get_categories($args);

    // Start output buffer
    ob_start();

    // Check if there are any categories with posts
    $categories_with_posts = array_filter($categories, function ($category) {
        $category_posts_args = array(
            'post_type' => 'job-position', // replace with your custom post type
            'posts_per_page' => 1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'job-positions-category',
                    'field' => 'term_id',
                    'terms' => $category->term_id,
                ),
            ),
        );
        $category_posts = get_posts($category_posts_args);
        return !empty($category_posts);
    });

    if ($categories_with_posts) {
        echo '<ul class="cate_ul">';
        foreach ($categories_with_posts as $category) {
            $category_link = get_term_link($category, 'job-positions-category');
            // Check if the category is the current one
            $active_class = ($current_category_id == $category->term_id) ? 'active' : '';

            // Output the category with the active class
            echo '<li class="' . esc_attr($active_class) . '">
                <a class="category-list-custom-item-url">' . esc_html($category->name) . '</a>';

            // Fetch and display posts for the current category
            $category_posts_args = array(
                'post_type' => 'job-position', // replace with your custom post type
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'job-positions-category',
                        'field' => 'term_id',
                        'terms' => $category->term_id,
                    ),
                ),
            );
            $category_posts = get_posts($category_posts_args);

            if ($category_posts) {
                echo '<ul class="post_ul">';
                foreach ($category_posts as $post) {
                    echo '<li><div class="job-position-row"><a class="job-position-row-link" href="' . esc_url(get_permalink($post->ID)) . '"><span>' . esc_html($post->post_title) . '</span><span>' . esc_html($post->type) . '</span><span><svg xmlns="http://www.w3.org/2000/svg" width="11" height="16" viewBox="0 0 11 16" fill="none">
<path d="M5.5 0.5C2.45929 0.5 0 2.8475 0 5.75C0 9.6875 5.5 15.5 5.5 15.5C5.5 15.5 11 9.6875 11 5.75C11 2.8475 8.54071 0.5 5.5 0.5ZM5.5 7.625C4.41571 7.625 3.53571 6.785 3.53571 5.75C3.53571 4.715 4.41571 3.875 5.5 3.875C6.58429 3.875 7.46429 4.715 7.46429 5.75C7.46429 6.785 6.58429 7.625 5.5 7.625Z" fill="#253D4E"/>
</svg>&nbsp;&nbsp;' . esc_html($post->address) . '</span></a><a class="job-position-row-detail-link elementor-button" href="' . esc_url(get_permalink($post->ID)) . '">Find out more</a></div></li>';
                }
                echo '</ul>';
            }


            echo '</li>';
        }
        echo '</ul>';
    } else {
        echo 'No categories found with posts.';
    }

    // End output buffer and return content
    return ob_get_clean();
});

add_shortcode('post_days_ago', function ($atts) {
    $atts = shortcode_atts(array(
        'id' => get_the_ID(),
    ), $atts, 'post_days_ago');

    $post_date = get_the_time('U', $atts['id']);
    $current_date = current_time('timestamp');
    $days_ago = 'Posted ' . ucwords(human_time_diff($post_date, $current_date)) . ' ago';

    return $days_ago;
});

add_shortcode('login-social', function () {
    echo '<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide pw-row tgb-social-login-container">
    <div class="login-social">
        <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40" fill="none">
                <ellipse cx="20" cy="18.3999" rx="17.5" ry="17.5" fill="url(#paint0_linear_2388_29212)" />
                <path d="M26.5171 25.3519L27.2945 20.4126H22.4315V17.2087C22.4315 15.8571 23.1096 14.5388 25.2877 14.5388H27.5V10.3337C27.5 10.3337 25.4931 10 23.5753 10C19.5685 10 16.9521 12.3662 16.9521 16.6481V20.4126H12.5V25.3519H16.9521V37.2931C17.8459 37.4299 18.7603 37.5 19.6918 37.5C20.6233 37.5 21.5377 37.4299 22.4315 37.2931V25.3519H26.5171Z" fill="white" />
                <defs>
                    <linearGradient id="paint0_linear_2388_29212" x1="20" y1="0.899902" x2="20" y2="35.7961" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#18ACFE" />
                        <stop offset="1" stop-color="#0163E0" />
                    </linearGradient>
                </defs>
            </svg>
        </a>
    </div>
    <div class="login-social">
        <a href="https://bonaecom.com/user12/unidbox/wp-login.php?loginSocial=google&redirect=https%3A%2F%2Fbonaecom.com%2Fuser12%2Funidbox%2Fwp-admin%2F">
            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="32" viewBox="0 0 30 32" fill="none">
                <path d="M28.1267 16.2987C28.1267 15.1926 28.0373 14.3854 27.8439 13.5483H15.2695V18.5409H22.6504C22.5017 19.7816 21.6981 21.6501 19.9123 22.9057L19.8873 23.0728L23.8631 26.1667L24.1386 26.1943C26.6683 23.8474 28.1267 20.3944 28.1267 16.2987Z" fill="#4285F4" />
                <path d="M15.269 29.4531C18.885 29.4531 21.9206 28.2572 24.138 26.1945L19.9118 22.9058C18.7808 23.6981 17.263 24.2511 15.269 24.2511C11.7273 24.2511 8.72141 21.9044 7.64988 18.6606L7.49282 18.674L3.35875 21.8879L3.30469 22.0389C5.50704 26.4336 10.0308 29.4531 15.269 29.4531Z" fill="#34A853" />
                <path d="M7.64874 18.6607C7.36601 17.8236 7.20238 16.9267 7.20238 16C7.20238 15.0731 7.36601 14.1763 7.63387 13.3392L7.62638 13.1609L3.44049 9.89551L3.30354 9.96094C2.39584 11.7846 1.875 13.8325 1.875 16C1.875 18.1674 2.39584 20.2152 3.30354 22.0389L7.64874 18.6607Z" fill="#FBBC05" />
                <path d="M15.269 7.74871C17.7839 7.74871 19.4803 8.83991 20.4476 9.75179L24.2273 6.04469C21.906 3.87725 18.8851 2.54688 15.269 2.54688C10.0309 2.54688 5.50705 5.56633 3.30469 9.96098L7.63504 13.3393C8.72144 10.0956 11.7274 7.74871 15.269 7.74871Z" fill="#EB4335" />
            </svg>
        </a>
    </div>
</div>';
});

add_shortcode('print-login-form', function () {
    ob_start(); // Start output buffering
    if( !is_user_logged_in() ) {
    do_action("woocommerce_before_customer_login_form"); 

?>
    <style>
        .col-form-login {
            width: 100%;
            max-width: 600px;
            padding: 30px;
            background-color: #fff;
            border-radius: 15px;    
            margin-bottom: 50px;
            margin-top: 50px;
        }

        .col-form-login h2.title-form-login {
            text-align: center;
            font-size: 36px; 
            font-weight:700;
        }

        .col-form-login form .woocommerce-form-login__submit {
            margin-bottom: 20px;
        }

        .col-form-login form input {
            border: 1px solid var(--e-global-color-29aed8d);
        }

        .col-form-login .woocommerce-form-login .woocommerce-form-login__submit {
            float: none !important;
            width: 100%;
            font-size: 16px;
        }
        .guest-checkout-form-description {
            font-size: 16px;
            display: none;
            color: var(--e-global-color-d85c849, #253D4E);
        }
        .woocommerce-form-row {
            margin-bottom: 24px;
        }

        @media (max-width: 880px) {
            .col-form-login h2.title-form-login {
                text-align: left;
                font-size: 28px; 
            }
        }
    </style>
    <div class="col-form-login" id="customer_login">
        <h2 class="title-form-login elementor-size-default"><?php esc_html_e("Sign In", "woocommerce"); ?></h2>
        <p class="elementor-size-default guest-checkout-form-description"><?php esc_html_e("Sign in for faster checkout and to redeem and earn rewards!", "woocommerce"); ?></p>
        <form class="woocommerce-form woocommerce-form-login login" method="post">
            <?php do_action("woocommerce_login_form_start"); ?>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="username"><?php esc_html_e("Email Address", "woocommerce"); ?>&nbsp;<span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="Enter your email address" name="username" id="username" autocomplete="username" value="<?php echo (!empty($_POST["username"])) ? esc_attr(wp_unslash($_POST["username"])) : ""; ?>" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide pw-row">
                <label for="password"><?php esc_html_e("Password", "woocommerce"); ?>&nbsp;<span class="required">*</span></label>
                <input class="woocommerce-Input woocommerce-Input--text input-text" placeholder="Enter your Password" type="password" name="password" id="password" autocomplete="current-password" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide pw-row">
                <span class="forgot-password-text"><a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e("Forgot Password?", "woocommerce"); ?></a></span>
            </p>
            <?php do_action("woocommerce_login_form"); ?>

            <p class="form-row">
                <?php wp_nonce_field("woocommerce-login", "woocommerce-login-nonce"); ?>
            <div class="submit-losspw">
                <button type="submit" class="elementor-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e("Sign In", "woocommerce"); ?>"><?php esc_html_e("Sign in", "woocommerce"); ?></button>
<!--                 <p>Do not have an account yet? <a href="<?php echo home_url(); ?>/register">Sign Up</a></p> -->
            </div>
			<script src="https://www.google.com/recaptcha/api.js?render=6LeO1lAnAAAAAKQk8sj6bJVc8oCVh1OsSnHWvIj4"></script>
            <script>
                grecaptcha.ready(function () {
                    grecaptcha.execute('6LeO1lAnAAAAAKQk8sj6bJVc8oCVh1OsSnHWvIj4', {action: 'register'}).then(function (token) {
                        document.getElementById('g-recaptcha-response').value = token;
                    });
                });
            </script>
            <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
            </p>
            <?php do_action("woocommerce_login_form_end"); ?>
            <?php echo do_shortcode("[login-social]"); ?>
        </form>
    </div>
<?php
}
else {
    ?>
    <div class="col-form-login" id="customer_login">
    <p style="text-align: center">You are logged in!</p>
    </div>
    <?php
}
    return ob_get_clean(); // Return buffered content
});
