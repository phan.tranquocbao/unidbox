<?php
// Custom quantity field
function ts_quantity_plus_minus() {
	// To run this on the single product page
	if ( ! ( is_product() || is_cart() ) ) return;
	?>
	<script type="text/javascript">
		jQuery(document).ready(function($){   
			$('form.cart, form.woocommerce-cart-form ').each(function() {
				// Get current quantity input
				var qtyInput = $(this).find('.qty');
				var btn_update_cart = $('.woocommerce-cart-form').find('button[name="update_cart"]');
				// Create the plus and minus buttons
				var plusButton = '<input type="button" class="qty_button plus" value="+">';
				var minusButton = '<input type="button" class="qty_button minus" value="-">';
				qtyInput.parent('.quantity').prepend(minusButton).append(plusButton);
				
				// Add click event for buttons
				$(this).on('click', 'input.plus, input.minus', function() {
					var val = parseFloat(qtyInput.val());
					var max = parseFloat(qtyInput.attr('max'));
					var min = parseFloat(qtyInput.attr('min'));
					var step = parseFloat(qtyInput.attr('step'));
					btn_update_cart.prop('disabled', false);
					if ($(this).hasClass('plus')) {
						if (max && max <= val) {
							qtyInput.val(max);
						} else {
							qtyInput.val(val + step);
						}
					} else {
						if (min && min >= val) {
							qtyInput.val(min);
						} else if (val > 1) {
							qtyInput.val(val - step);
						}
					}
				});
			});
		});
	</script>
	<?php
}

// Remove the old actions
// remove_action('woocommerce_after_add_to_cart_quantity', 'ts_quantity_plus_sign');
// remove_action('woocommerce_before_add_to_cart_quantity', 'ts_quantity_minus_sign');

// Add the new action to inject the buttons and JavaScript
// add_action('woocommerce_after_add_to_cart_quantity', 'ts_quantity_plus_minus');
// add_action('woocommerce_before_cart', 'ts_quantity_plus_minus');

add_action( 'wp_footer' , function(){
	// To run this on the single product page
	if ( ! is_cart()  ) return;
	?>
	<script>
		$(document).ready(function() {
			$(document).on("click","#apply_coupon_check",function() {
				var coupon_code = $('#coupon_code_check').val();
				$('#coupon_code').val(coupon_code);
				if ($('#coupon_code').val() === coupon_code) {
					$('#apply_coupon').trigger('click');
				}
			});
		});
	</script>
<?php
} );


add_action( 'wp_footer' , function(){
	?>
    <script type='text/javascript'>
    jQuery( function( $ ) {
        if ( ! String.prototype.getDecimals ) {
            String.prototype.getDecimals = function() {
                var num = this,
                    match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if ( ! match ) {
                    return 0;
                }
                return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
            }
        }
        // Quantity "plus" and "minus" buttons
        $( document.body ).on( 'click', 'input.plus, input.minus', function() {
            var $qty        = $( this ).closest( '.quantity' ).find( '.qty'),
                currentVal  = parseFloat( $qty.val() ),
                max         = parseFloat( $qty.attr( 'max' ) ),
                min         = parseFloat( $qty.attr( 'min' ) ),
                step        = $qty.attr( 'step' );

            // Format values
            if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
            if ( max === '' || max === 'NaN' ) max = '';
            if ( min === '' || min === 'NaN' ) min = 0;
            if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

            // Change the value
            if ( $( this ).is( '.plus' ) ) {
                if ( max && ( currentVal >= max ) ) {
                    $qty.val( max );
                } else {
                    $qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
                }
            } else {
                if ( min && ( currentVal <= min ) ) {
                    $qty.val( min );
                } else if ( currentVal > 0 ) {
                    $qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
                }
            }

            // Trigger change event
            $qty.trigger( 'change' );
        });
    });
    </script>
    <?php
} );


add_shortcode('cart_button_custom', function(){
	$icon = '<svg xmlns="http://www.w3.org/2000/svg" width="32" height="30" viewBox="0 0 32 30" fill="none"><path d="M12.3333 28.9997C13.0697 28.9997 13.6667 28.4027 13.6667 27.6663C13.6667 26.93 13.0697 26.333 12.3333 26.333C11.597 26.333 11 26.93 11 27.6663C11 28.4027 11.597 28.9997 12.3333 28.9997Z" stroke="#253D4E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path><path d="M27.0003 28.9997C27.7367 28.9997 28.3337 28.4027 28.3337 27.6663C28.3337 26.93 27.7367 26.333 27.0003 26.333C26.2639 26.333 25.667 26.93 25.667 27.6663C25.667 28.4027 26.2639 28.9997 27.0003 28.9997Z" stroke="#253D4E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path><path d="M1.66699 1H7.00033L10.5737 18.8533C10.6956 19.4672 11.0295 20.0186 11.5171 20.4111C12.0046 20.8035 12.6146 21.012 13.2403 21H26.2003C26.8261 21.012 27.4361 20.8035 27.9236 20.4111C28.4111 20.0186 28.7451 19.4672 28.867 18.8533L31.0003 7.66667H8.33366" stroke="#253D4E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path></svg>';
	$cart_quantity = WC()->cart->get_cart_contents_count();
	$button = '
	<div class="elementor-icon-wrapper elementor-menu-cart__toggle_button">
		<a class="elementor-icon elementor-animation-" href="'.home_url().'/cart">
			'.$icon.'
		</a>
		<span class="elementor-button-icon-qty">'. $cart_quantity . '</span>
	</div>';
	return $button;
});

add_action( 'template_redirect', function() {
	global $wp;
	if ( is_checkout() && !empty( $wp->query_vars['order-received'] ) && empty( $_GET['popup'] )) {
			$order_id = absint( $wp->query_vars['order-received'] );
			$order    = wc_get_order( $order_id );
			if ( $order && ! is_wp_error( $order ) && ! $order->has_status( 'failed' ) ) {
					$hashedsuccess = md5( 'checkout!Unidbox@@success' );
					// $redirect_url  = home_url( '/checkout/order-received/' . $order_id . '/?key=' . $_GET['key'] . '&popup=' . $hashedsuccess );
					$redirect_url = home_url('/thankyou-order/?order_id='. $order_id.'&key='. $_GET['key'] . '&popup='. $hashedsuccess);
					wp_redirect( $redirect_url );
					exit;
		} else {
			wp_redirect( home_url() );
			exit;
		}
	}
	if(is_checkout() && !empty( $wp->query_vars['order-received'] ) && !empty( $_GET['popup'] )){
		if($_GET['popup'] !=  md5( 'checkout!Unidbox@@success' )){
			wp_redirect( home_url() );
			exit;
		}
	}
});

