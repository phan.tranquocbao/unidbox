<?php
require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';
// Show short description on loop product
add_action( 'woocommerce_after_shop_loop_item_title', function(){
    global $product;
    $short_description = $product->get_short_description();
    if ( ! empty( $short_description ) ) {
        echo '<div class="product-short-description">' . wp_kses_post( $short_description ) . '</div>';
    }
}, 5 );

add_shortcode('product_reviews', function(){
    ob_start();
    wc_get_template('single-product-reviews.php');
    return ob_get_clean();
});

add_shortcode('custom_filter_price_product_plugin_annasta_woo_filter', function(){ 
    $id_price_range = "shortcode-1-filter-1-2-wrapper";
    $script = script_custom_filter_price_product_plugin_annasta_woo_filter($id_price_range);
    return $script;
});

function script_custom_filter_price_product_plugin_annasta_woo_filter($id_price_range) {
    ob_start();
    ?>
    <script>
        jQuery(document).ready(function($) {
            var priceRangeElement = $('#<?php echo esc_attr($id_price_range); ?>');
            if (priceRangeElement.length) {
                var targetDivLower = $('.noUi-handle-lower > .noUi-tooltip');
                var targetDivUpper = $('.noUi-handle-upper > .noUi-tooltip');
                var textLower = targetDivLower.text();
                var textUpper = targetDivUpper.text();

                var awfFiltersContainer = priceRangeElement.find('.awf-filters-container');
                var newDiv = $('<div>').addClass('cus_number_price_filter').attr('id', '<?php echo esc_attr($id_price_range . '_cus'); ?>');
                newDiv.append('<span class="min_price_cus">'+textLower+'</span><span>-</span><span class="max_price_cus">'+textUpper+'</span>');

                // Find the .awf-apply-filter-btn
                var applyFilterBtn = awfFiltersContainer.find('.awf-apply-filter-btn');

                // Insert newDiv before the applyFilterBtn
                newDiv.insertBefore(applyFilterBtn);


                // Lower
                var observer = new MutationObserver(function(mutations) {
                    mutations.forEach(function(mutation) {
                        newDiv.find('.min_price_cus').text(targetDivLower.text());
                    });
                });
                var observerConfig = { childList: true, subtree: true };
                observer.observe(targetDivLower[0], observerConfig);


                // Upper
                var observer = new MutationObserver(function(mutations) {
                    mutations.forEach(function(mutation) {
                        newDiv.find('.max_price_cus').text(targetDivUpper.text());
                    });
                });
                var observerConfig = { childList: true, subtree: true };
                observer.observe(targetDivUpper[0], observerConfig);


            }
            


        });
    </script>
    <?php
    return ob_get_clean();
}
