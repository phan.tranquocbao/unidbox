<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'unidbox' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J+DzA=C7uC57#KUyJoImjYp/]@(]Gwd$)5Q>4W53$-yMT<g<j;~cf<c4aq@}^]$b' );
define( 'SECURE_AUTH_KEY',  'g;OCmnREdep}%jOZM5N-fIl=mYtU,2`O/p*pAj>0a7N^[b{lNc=][ k0:tOJ>M?f' );
define( 'LOGGED_IN_KEY',    'zZ7=;(Rt<[(HDCR8houy})62puJks!ZJs}B;]v#z_Xv=;kb,izC#fLvEjC}6P+38' );
define( 'NONCE_KEY',        'n V#$-W|:MH]kRBw6jq()i2dsgh%d8LKS=I+`4uN0hs%Xf}g52K`<x]jirU:G[z|' );
define( 'AUTH_SALT',        'FqLB4J*dM9SiAB(<85Q$si =H{`mY&>{A+7B/]>UQTj63`?)1HEO]|ZR1b?zF.+U' );
define( 'SECURE_AUTH_SALT', '$)T,Qr<SQp_:&T2p}UL{Wn.Qd)OY.WyW8U-s.h}CS;/@C76]+<+^0ZDOiY8=w2@k' );
define( 'LOGGED_IN_SALT',   ':c[8%.^9!?j8ora! &O[ jS</Nd+RBDBp[{IbAYd5KzTbH17^R1tb)K_<Tim8{~:' );
define( 'NONCE_SALT',       '24:hln7~VT1jortWlHYgM!leK:x-Az!1!a*=MNO9Z0ER*:bybj*}Y!`tWF={}%fO' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
